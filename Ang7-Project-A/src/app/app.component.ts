﻿import { Component, Inject, HostListener, ViewChild } from '@angular/core';
import { DOCUMENT, Title } from '@angular/platform-browser';
import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';
import { AppGlobals } from './app.global';
import { environment } from '../environments/environment';
import { AuthenticationService, DataService, RingcentralService } from './_services/index';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Subscription } from 'rxjs';

@Component({
    moduleId: module.id.toString(),
    selector: 'app',
    templateUrl: 'app.component.html'
})

export class AppComponent {
  	
    idleState = 'Not started.';
    timedOut = false;
    lastPing: Date = null;

    deviceDetail = {};
    maintenance = {};

    themeColor = {
      '#08aeea': 'blue-theme',
      '#fd7777': 'red-theme',
      '#fdb869': 'orange-theme',
      '#fde669': 'yellow-theme',
      '#5cb9ac': 'green-theme',
    };

    private currentUrl: string = '';
    private previousUrl: string = '';
    private company_id: string = '';
    currentUser: { [index:string] : any } = {};
    contractSaveResEventsubscription : Subscription;

  	constructor(private dataService: DataService, private deviceService: DeviceDetectorService, private idle: Idle, private keepalive: Keepalive, private title: Title, @Inject(DOCUMENT) private _document: HTMLDocument, private router : Router, private _global: AppGlobals, private authenticationService: AuthenticationService, private ringCentralService: RingcentralService) {
      
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.deviceDetail = this.deviceService.getDeviceInfo();

    }

    reset() {
      if ( localStorage.getItem('currentUser') != undefined && localStorage.getItem('currentUser') != null ) {
        this.idle.watch();
        this.idleState = 'Started.';
        this.timedOut = false;
      }
    }

  	ngOnInit() {

  		//document.documentElement.style.setProperty('$primary', 'green');
      this.currentUrl = this.router.url;
  		this.router.events.forEach((event: NavigationEvent) => {
          
          // Reset idle watch
          this.reset();

          //Before Navigation
          if (event instanceof NavigationStart) {
              //console.log('start', event.url);
          }

          //After Navigation
          if (event instanceof NavigationEnd) {
            
            this.previousUrl = this.currentUrl;
            this.currentUrl = event.url;
            
            if ( (this.currentUser != null && this.currentUser['user_role'] != 'customer') && (this.currentUrl.indexOf('providers/') !== -1 || this.currentUrl.indexOf('invoices') !== -1 || this.currentUrl.indexOf('investors/') !== -1)) {
              this._document.body.classList.add('provider-panel');
            } else {
              if ( this.previousUrl.indexOf('providers/') !== -1 || this.previousUrl.indexOf('invoices') !== -1 || this.currentUrl.indexOf('investors/') !== -1 ) {
                this._document.body.classList.remove('provider-panel');
              }
            }

            if ( this.currentUrl.indexOf('companies/') !== -1 ) {
              this._document.body.classList.add('company-panel');
            } else {
              if ( this.previousUrl.indexOf('companies/') !== -1 ) {
                this._document.body.classList.remove('company-panel');
              }
            }

            // remove overflow-hidden class from body
            this._document.body.classList.remove('overflow-hidden');

            // Set dynamic title
            this.title.setTitle(this._global.getlocalStorage('provider_name'));

            // Set dynamic favicon
            if ( this._global.getlocalStorage('favicon') != undefined && this._global.getlocalStorage('favicon') != null ) {
              this._document.getElementById('appFavicon').setAttribute('href', this._global.getlocalStorage('favicon'));
            } else {
              this._document.getElementById('appFavicon').setAttribute('href', '');
            }

            // Set dynamic primary and dark color
            if ( this._global.getlocalStorage('theme_color') != undefined && this._global.getlocalStorage('theme_color') != null ) {
              let customStyle = "--primary: " + this._global.getlocalStorage('theme_color') + ";";
              customStyle += " --secondary: " + this._global.adjustBrightness(this._global.getlocalStorage('theme_color'), -55) + ";";
              document.querySelector("body").style.cssText = customStyle;
            } else {
              document.querySelector("body").style.cssText = '';
            }

            // set default theme for ie
            if ( this.deviceDetail['browser'] == 'IE' ) {
              document.body.classList.add('default-theme');
              if ( this._global.getlocalStorage('theme_color') != undefined && this._global.getlocalStorage('theme_color') != null ) {
                document.body.classList.add(this.themeColor[this._global.getlocalStorage('theme_color')]);
              }
            } else {
              document.body.classList.remove('default-theme');
            }

            // set maintenance class
            this.maintenance = JSON.parse(localStorage.getItem('maintenance_mode'));
            if ( this.maintenance != undefined ) {
              if ( this.maintenance['is_maintenance_mode'] ) {
                document.body.classList.add('maintenance');
              } else {
                document.body.classList.remove('maintenance');
              }
            } else {
              document.body.classList.remove('maintenance');
            }

            if ( this.currentUrl.indexOf('login') !== -1 ) {
              this._document.body.classList.add('full-page-layout');
            } else {
              this._document.body.classList.remove('full-page-layout');
            }
            
          }
      });

    }

    @HostListener('window:message', ['$event'])
    onMessage(event) {
      if ( this.currentUser != null && this.currentUser != undefined ) {
        if ( this.currentUser.user_role == "superadmin" || this.currentUser.user_role == "admin" ) {
          const data = event.data;
          if (data) {
            if (data.type === 'rc-adapter-pushAdapterState' && this.ringCentralService._isRingCentralRegistered === false) {
              this.ringCentralService._isRingCentralRegistered = true;
            } else if (data.type === 'rc-call-ring-notify') {
              // get call when user gets a ringing call
              
              const fromNumber = data.call.from;
              if ( this.currentUrl.indexOf('/contracts/add') !== -1 || this.currentUrl.indexOf('/contracts/edit') !== -1 ) {
                this.ringCentralService.contractSaveEvent();
                setTimeout(() => {
                  this.ringCentralService.getContractByCustomerNumber(fromNumber);
                }, 10);
              } else {
                this.ringCentralService.getContractByCustomerNumber(fromNumber);
              }
            }
          }
        }
      }
    }

    logout() {
      // reset login status
      this.authenticationService.logout()
          .subscribe( data => {
               this.router.navigate(['/login']); 
            },
            error => { 
               console.log(error);
            }
          );
    }
}
