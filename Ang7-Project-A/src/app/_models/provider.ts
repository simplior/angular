export class Provider {
  lwp_program: number;
  epo_days: number;
  provider_name: string;
  email:string;
  portal_url: string;
}
