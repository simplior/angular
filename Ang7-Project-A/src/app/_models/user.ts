﻿export class User {
    id: number;
    userRole: [
      {roleType: 'Admin'},
      {roleType: 'Support'},
      {roleType: 'Agent'}
    ];
    profile_image:string
    user_role:string;
    provider_name:string;
    provider_logo:string;
    favicon:string;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    selected: boolean;
}
