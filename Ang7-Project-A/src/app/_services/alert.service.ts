import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable ,  Subject } from 'rxjs';
import {isBoolean} from 'util';

@Injectable()
export class AlertService {
    private subject = new Subject<any>();
    private keepAfterNavigationChange = false;

    constructor(private router: Router) {
        // clear alert message on route change
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    this.keepAfterNavigationChange = false;
                } else {
                    // clear alert
                    this.subject.next();
                }
            }
        });
    }

    success(message: string, keepAfterNavigationChange , status: string = 'true') {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message, status: status });
    }

    error(message: string, keepAfterNavigationChange , status: string = 'false') {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message, status: status });
    }

    confirm(message: string, keepAfterNavigationChange , status: string = 'true', siFn:()=>void, noFn:()=>void) {
        $('alert').show();
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        let that = this;
        this.subject.next({ 
            type: 'confirm', 
            text: message,
            status: status,
            siFn: function(){
                that.subject.next();
                siFn();
            },
            noFn:function(){
                that.subject.next();
                noFn();
            }
        });
    }

    doubleconfirm(message: string, keepAfterNavigationChange , status: string = 'true', siFn:()=>void, si2Fn:()=>void, noFn:()=>void) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        let that = this;
        this.subject.next({ 
            type: 'doubleconfirm', 
            text: message,
            status: status,
            siFn: function(){
                that.subject.next();
                siFn();
            },
            si2Fn: function(){
                that.subject.next();
                si2Fn();
            },
            noFn:function(){
                that.subject.next();
                noFn();
            },
        });
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

    clearMessage() {
        this.subject.next();
    }
}
