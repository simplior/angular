import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { AppGlobals } from '../app.global';
import { DataService } from './data.service';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RingcentralService {
  
  private subject = new Subject<any>();
  _isRingCentralRegistered: boolean = false;

  get isRingCentralRegistered(): boolean {
      return this._isRingCentralRegistered;
  }

  set isRingCentralRegistered(val: boolean) {
      this._isRingCentralRegistered = val;
  }

  contractSaveEvent() {
    this.subject.next();
  }
  
  getContractSaveEvent(): Observable<any>{ 
    return this.subject.asObservable();
  }

  constructor(private dataService: DataService, private _global: AppGlobals, @Inject(DOCUMENT) private _document: HTMLDocument, private router : Router) { }

  ringcentralAction(mode = 'call', data) {
    if ( this._isRingCentralRegistered && data.contact_no != '' && data.contact_no != null ) {
      const element = this._document.getElementById('rc-widget-adapter-frame') as HTMLIFrameElement;
      element.contentWindow.postMessage({
        type: mode,
        // type: 'rc-adapter-new-call',
        // type: 'rc-adapter-new-sms',
        phoneNumber: '+1'+data.contact_no,
        toCall: true,
      }, '*');
    }
  }

  getContractByCustomerNumber(fromNumber){
    this.dataService.get('getContractByCustomerNumber/'+fromNumber)
      .subscribe(contracts => {
          if ( contracts.success && contracts.data.length > 0) {
            this.router.navigate(['/contracts'], { queryParams: {s: contracts.message, callFrom: 'ringCentral'}});
          }
      }, reject => {
        console.log(reject);
      });
  }
}
