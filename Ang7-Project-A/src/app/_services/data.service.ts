import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppGlobals } from '../app.global';
import 'rxjs/add/operator/map';
import { BehaviorSubject ,  Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable()
export class DataService {
    baseAPIUrl: string = "";
    constructor(private http: HttpClient,  private router: Router) { 
        this.baseAPIUrl = environment.apiUrl;
    }

    post(url: string, data = null): Observable<any> {
        return this.http.post<any>(this.baseAPIUrl + url, JSON.stringify(data)/*, {'headers': headers}*/)
            .pipe(
               //retry(1),
               catchError(this.handleError)
            )
        	.map(res => {
                return res;
            });
    }

    get(url: string) {
        return this.http.get<any>(this.baseAPIUrl + url)
      		.pipe(
               //retry(1),
               catchError(this.handleError)
            ).map(res => {
                return res;
            });
    }

    put(url: string, data = null) {
        return this.http.put<any>(this.baseAPIUrl + url, JSON.stringify(data))
        	.pipe(
               //retry(1),
               catchError(this.handleError)
            ).map(res => {
                return res;
            });
    }

    delete(url: string) {
        return this.http.delete<any>(this.baseAPIUrl + url)
        	.pipe(
               //retry(1),
               catchError(this.handleError)
            ).map(res => {
                return res;
            });
    }

    smartystreets(url: string) {
        return this.http.get<any>(url)
            .map(res => {
                return res;
            });
    }

    handleError(error) {
       if ( error.error.error != undefined && error.error.error == 'Unauthenticated.' ) {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('permissions');
       }
       return throwError(error);
    }
}
