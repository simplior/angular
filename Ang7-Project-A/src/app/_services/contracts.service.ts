import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppGlobals } from '../app.global';


@Injectable()
export class ContractsService {

  constructor(private http: HttpClient,private _global: AppGlobals) { }

  getFlashData(){
    return this.http.get(this._global.baseAPIUrl+'contract/getflashdata');
  }
  getUsersData(){
    return this.http.get(this._global.baseAPIUrl+'contract/getuserdata');
  }

  getContractData(status:null)
  {
    if(status)
    {
      return this.http.get(this._global.baseAPIUrl+'contract'+ '?status='+status); 
    }
    else{
      return this.http.get(this._global.baseAPIUrl+'contract') ;
    }
   
  }
}
