
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AppGlobals } from '../app.global';
import { environment } from './../../environments/environment';
import { env } from 'process';

@Injectable()
export class AuthenticationService {

    currentUser;
    private headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
    });

    constructor(private http: HttpClient,private _global: AppGlobals) {
        /*this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if ( this.currentUser != undefined ) {
            let old_token = this.headers.get('Authorization');
            this.headers.append('Authorization', 'Bearer '+ this.currentUser.token);
        }*/
    }

    login(user_role: string, username: string, password: string, remember_me: any, login_with: any) {
        this.headers.append('Provider-Code', '');
        return this.http.post<any>(environment.apiUrl + 'login', JSON.stringify({user_role: user_role, username: username, password: password, login_with: login_with}), {'headers': this.headers}).pipe(
            map(user => {
                // login successful if there's a jwt token in the response
                if (user.success) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user.data));
                    localStorage.setItem('permissions', JSON.stringify(user.permissions));
                    localStorage.setItem('maintenance_mode', JSON.stringify(user.maintenance_mode));
                    localStorage.setItem('contract_columns', JSON.stringify(user.contract_columns));

                    if ( remember_me ) {
                        localStorage.setItem('remember_me', JSON.stringify({
                            'remember_me': true,
                            'login_with': login_with,
                            'username': username,
                            'password': password,
                        }));
                    } else {
                        localStorage.removeItem('remember_me');
                    }
                }
                return user;
            }));
    }

    forgotPassword(user_role: string, username: string, login_with: any) {
        this.headers.append('Provider-Code', '');
        return this.http.post<any>(environment.apiUrl + 'password/create', { user_role: user_role, email_id: username, login_with: login_with}, {'headers': this.headers});
    }
    
    resetPassword(password: string, confirmPassword: string, validateKey:string ) {
        this.headers.append('Provider-Code', '');
        return this.http.post<any>(environment.apiUrl + 'password/reset',
        JSON.stringify({ password: password, confirmPassword: confirmPassword, token: validateKey}), {'headers': this.headers});
    }
    
    validateKey(validateKey: string) {
        this.headers.append('Provider-Code', '');
        return this.http.get<any>(environment.apiUrl + 'password/find/' + validateKey, {'headers': this.headers});
    }
    
    logout() {
        this.headers.append('Provider-Code', '');
        // remove user from local storage to log user out
        return this.http.post<any>(environment.apiUrl + 'logout', {}, {'headers': this.headers}).pipe(
            map(user => {
                this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
                
                let pastUser = {};
                pastUser['logo'] = this.currentUser.provider_logo;
                pastUser['provider_name'] = this.currentUser.provider_name;
                pastUser['favicon'] = this.currentUser.favicon;
                pastUser['theme_color'] = this.currentUser.theme_color;

                //localStorage.setItem('pastUser', JSON.stringify(pastUser));
                localStorage.removeItem('currentUser');
                localStorage.removeItem('permissions');
                localStorage.removeItem('contract_columns');
                return user;
            }));
    }
}
