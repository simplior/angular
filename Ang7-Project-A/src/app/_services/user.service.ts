﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/index';
import { AppGlobals } from '../app.global';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UserService {
    constructor(private http: HttpClient,private _global: AppGlobals) { }

    private messageSource = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    currentMessage = this.messageSource.asObservable();

    changeMessage(message: any) {
      //this.messageSource.next(message);
    }

    /*getAll(role = null) {
        if(role) {
            return this.http.get<User[]>(this._global.baseAPIUrl + 'users?role='+role);
        } else{
            return this.http.get<User[]>(this._global.baseAPIUrl + 'users');
        }
    }*/

    getAll(data = null) {
        return this.http.post<any>(this._global.baseAPIUrl + 'users', data);
    }

    getById(id: number) {
        return this.http.get('http://localhost:4000/users/' + id);
    }

    addUser(user: User) {
        return this.http.post<any>(this._global.baseAPIUrl + 'users/add', user);
    }

    addProvider(user: User) {
      return this.http.post<any>(this._global.baseAPIUrl + 'users/addProvider', user);
    }
    update(user: User) {
        return this.http.put('http://localhost:4000/users/' + user.id, user);
    }

    delete(user_id) {
        return this.http.post<any>(this._global.baseAPIUrl + 'users/delete',{ ids: user_id});
    }

    bulkAction(data){
        if ( data['action'] == 'delete' ) {
            return this.http.post<any>(this._global.baseAPIUrl + 'users/delete', data);       
        } else {
            return this.http.post<any>(this._global.baseAPIUrl + 'users/changeStatus', data);
        }
    }

    getNotification(){
        return this.http.get('https://nfrapi.cwsefforts.com/notification');
    }

    getProfileDetails(){
      return this.http.get(this._global.baseAPIUrl + 'providers/getProfile');
    }

    getProfileSettings(){
      return this.http.get(this._global.baseAPIUrl + 'providers/getprofilesettings');
    }

    updateProfile(details){
      return this.http.post<any>(this._global.baseAPIUrl + 'providers/profileupdate', details);
    }

    updateBrand(details){
      return this.http.post<any>(this._global.baseAPIUrl + 'providers/brandupdate', details);
    }

    updateTerms(details){
      return this.http.post<any>(this._global.baseAPIUrl + 'providers/termsupdate', details);
    }
}
