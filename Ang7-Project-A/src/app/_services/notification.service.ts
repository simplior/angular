import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppGlobals } from '../app.global';
import { environment } from './../../environments/environment';

import * as io from 'socket.io-client';

@Injectable()
export class NotificationService {
	// private socket: SocketIOClient.Socket;
	private socket;
	private socketId;
	private port = 8081;
	socketDomain:string = '';
	isSocketSecure: boolean = true;

  	constructor(private _global: AppGlobals) {
		this.socketDomain = environment.socketDomain;
		this.isSocketSecure = environment.isSocketSecure;
	}
	
	// EMITTER
	sendEmitter(type = 'notifyToAll', data = {}) {
		
		// Notify to All
		if ( type == 'notifyToAll' ) {
			// this.socket.emit('notifyToAll', { data: data });
			
			// comment for stop notification
			// this.socket.emit('sendMessage', { data: data });
		}

		// Get Client Detail
		if ( type == 'getClientDetail' ) {
			// comment for stop notification
			// this.socket.emit('getClientDetail', { data: data });
		}
	}

	// HANDLER
	receivedNotification(type = 'notifyToAll') {

		if ( type == 'notifyToAll' ) {
			return Observable.create(observer => {
				this.socket.on('newMessage', msg => {
				  observer.next(msg);
				});
		 	});
		}
		
		if ( type == 'getClientDetail' ) {
			return [];
			return Observable.create(observer => {
				this.socket.on('resGetClientDetail', msg => {
				  observer.next(msg);
				});
		 	});
		}
	}

	// Close the current socket session manually
	close(){
		if( this.socket && typeof this.socket.close === "function" ){
			this.socket.close();
		}
		this.socket = undefined;
	}
}
