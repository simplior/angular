import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { AlertService, AuthenticationService, DataService, NotificationService } from '../_services/index';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
})
export class ErrorsComponent implements OnInit {

	data: { [index:string] : any } = {}
	
  	constructor(private activatedRoute: ActivatedRoute, private dataService: DataService) { }

  	ngOnInit() {
		this.activatedRoute.params.subscribe((params: Params) => {
	        this.data = params;
	    });
  	}

}
