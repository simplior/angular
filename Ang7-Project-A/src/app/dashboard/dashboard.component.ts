import { Component, OnInit, ViewChild } from '@angular/core';
import { PaymentCalculateComponent } from '../contracts/payment-calculate/payment-calculate.component';
import { AlertService, AuthenticationService, DataService, NotificationService } from '../_services/index';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AppGlobals } from '../app.global';
import { environment } from '../../environments/environment';

//import * as d3 from '../../assets/datamaps/d3.min.js';
//import * as topojson from '../../assets/datamaps/topojson.min.js';

import * as Datamap from '../../assets/datamaps/datamaps.usa.min.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})

export class DashboardComponent implements OnInit {

  currentUser: { [index:string] : any } = [];
  companyList = [];
  productList = [];
  productListIframe = [];
  stateList = [];
  errors: { [index:string] : any } = {};
  contractToCal: { [index:string] : any } = {};
  contractDownload: { [index:string] : any } = {};
  @ViewChild(PaymentCalculateComponent ) paymentCal: PaymentCalculateComponent ;

  showTermSection: boolean = false;

  contractsByTerms: any;
  contractsByTermsLoder: boolean = false;

  contractsByStatus: any;
  contractsByStatusLoder: boolean = false;

  contractsByProducts: any;
  contractsByProductsLoder: boolean = false;

  contractsByReferrals: any;
  contractsByReferralsLoder: boolean = false;
  
  contractsByAge: any;
  contractsByAgeLoder: boolean = false;
  
  contractsByLocation: any;
  contractsByLocationLoder: boolean = false;

  contractsByPurchased: any;
  contractsByPurchasedLoder: boolean = false;

  contractsByGenerated: any;
  contractsByGeneratedLoder: boolean = false;

  contractsByPurchasedAmount: any;
  contractsByPurchasedAmountLoder: boolean = false;

  contractsByGeneratedAmount: any;
  contractsByGeneratedAmountLoder: boolean = false;
  
  isSubmitRun: boolean = false;

  attachment_via_upload = 1;
  attachment64Data;
  attachmentImage;
  attachmentImageName;
  attachmentImageType;
  previewImage: boolean = false;

  imageurl = environment.uploadUrl;

  loading = false;
  loadingDown = false;
  exporting = false;
  iframe: { [index:string] : any } = {};
  itemNew: {};
  providerList = [];
  dealerList = [];
  fontList = [];
  copyToTxt = 'Copy Code';
  deviceDetail = {};

  chartOptions = {
    scales: {
      xAxes: [{
        ticks: {
          fontFamily: "Avenir-Next",
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero:true,
          fontFamily: "Avenir-Next",
          callback: function (value) { 
            if (Number.isInteger(value)) {
              return value;
            }
          },
          userCallback: function(label, index, labels) {
            if (Math.floor(label) === label) {
              return label;
            }
          },
        }
      }]
    },
    tooltips: {
      bodyFontFamily: "Avenir-Next",
    }
  };

  amountChartOptions = {
    scales: {
      xAxes: [{
        ticks: {
          fontFamily: "Avenir-Next",
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero:true,
          fontFamily: "Avenir-Next",
          /*callback: function(value, index, values) {
            return '$' + value;
          },*/
          userCallback: function(label, index, labels) {
            if (Math.floor(label) === label) {
              return '$' + label;
            }
          },
        }
      }]
    },
    tooltips: {
      bodyFontFamily: "Avenir-Next",
      callbacks: {
        label: function(tooltipItem) {
          return "Total Adjusted Cash: $" + Number(tooltipItem.yLabel);
        }
      }
    }
  }

  showCustomFiler: boolean = false;
  where = {'filertype': 'all'};
  public dateRangePicker: any = {
      singleDatePicker: false,
      autoUpdateInput: true,
      showDropdowns: true,
      drops: 'bottom',
      locale: {
        format: this._global.dateFormat
      },
  };
  
  use_map_data: any;

  constructor(private _global: AppGlobals, private deviceService: DeviceDetectorService, private dataService: DataService, private router: Router, private alertService: AlertService, private notificationService: NotificationService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    
    this.bindStateList();

    this.deviceDetail = this.deviceService.getDeviceInfo();

    if ( this.deviceDetail['browser'] == 'IE' ) {
      this.currentUser['theme_color'] = '#08aae6';
    }

    this.iframe.type = "contract_payment_calculator";
    if ( this.currentUser['user_role'] == 'admin' ) {
      this.iframe.provider_id = this.currentUser['provider_id'];
      this.getProducts();
    }

    if ( this.currentUser['user_role'] != 'superadmin' && this.currentUser['user_role'] != 'representative' ) {
      this.contractDownload.provider_id = this.currentUser['provider_id'];
      this.getProducts();
    }

    if ( this.currentUser['user_role'] == 'superadmin' || this.currentUser['user_role'] == 'admin' || this.currentUser['user_role'] == 'representative' || this.currentUser['user_role'] == 'company_admin' || this.currentUser['user_role'] == 'dealer' || this.currentUser['user_role'] == 'subdealer' ) {
      
      this.contractToCal.provider_id = null;
      if ( this.currentUser['user_role'] == 'admin' || this.currentUser['user_role'] == 'company_admin' || this.currentUser['user_role'] == 'dealer' || this.currentUser['user_role'] == 'subdealer' ) {
        this.contractToCal.provider_id = this.currentUser['provider_id'];
      }

      if ( this.currentUser['user_role'] == 'company_admin' || this.currentUser['user_role'] == 'dealer' || this.currentUser['user_role'] == 'subdealer' ) {
        this.contractToCal.company_id = this.currentUser['company_id'];
      }

      this.contractToCal.cash_price = '';
      this.contractToCal.tax_rate = '';
      this.contractToCal.is_lwp_value = 'yes';
      this.contractToCal.due_date = '';
      this.contractToCal.is_edp = false;
      this.contractToCal.edp_price = '';
      this.contractToCal.term = '';
      this.contractToCal.from_page = 'dashboard';

      if ( this.currentUser['user_role'] == 'subdealer' ) {
        this.contractToCal.created_by = this.currentUser['id'];
      }
      
      this.loadCharts();
    }

  }

  getProducts(){
    if ( this.contractDownload.provider_id != undefined ) {

      let dataPass:any = {'provider_id': this.contractDownload.provider_id};

      if ( this.currentUser['user_role'] == 'company_admin' || this.currentUser['user_role'] == 'dealer' || this.currentUser['user_role'] == 'subdealer' ) {
        dataPass.company_id = this.currentUser['company_id'];
      }

      if ( this.currentUser['user_role'] == 'dealer' || this.currentUser['user_role'] == 'subdealer' ) {
        if ( this.currentUser['user_role'] == 'subdealer' ) {
          dataPass.c_user_id = this.currentUser['parent_user_id'];
        } else {
          dataPass.c_user_id = this.currentUser['id'];
        }
      }
      this.dataService.post('productListFromUser', dataPass)
      .subscribe(product => {

          if ( product.success ) {
            this.productList = product.data;
            if ( Object.keys(this.productList).length > 0 ) {
              this.contractDownload.property_type = String(this.productList[0].ID);
            }
          }
      }, reject => {
          
          this.errors = reject.error.error;
      });
    }
  }

  getProductsIframe(){
    if ( this.iframe.provider_id != undefined ) {

      let dataPass:any = {'provider_id': this.iframe.provider_id};

      if ( this.currentUser['user_role'] == 'company_admin' || this.currentUser['user_role'] == 'dealer' || this.currentUser['user_role'] == 'subdealer' ) {
        dataPass.company_id = this.currentUser['company_id'];
      } else {
        dataPass.company_id = this.iframe.company_id;
      }

      if ( this.currentUser['user_role'] == 'dealer' || this.currentUser['user_role'] == 'subdealer' ) {
        if ( this.currentUser['user_role'] == 'subdealer' ) {
          dataPass.c_user_id = this.currentUser['parent_user_id'];
        } else {
          dataPass.c_user_id = this.currentUser['id'];
        }
      } else {
        dataPass.c_user_id = this.iframe.c_user_id;
      }

      this.dataService.post('productListFromUser', dataPass)
      .subscribe(product => {

          if ( product.success ) {
            this.productListIframe = product.data;
            if ( Object.keys(this.productListIframe).length > 0 ) {
              this.iframe.product_id = String(this.productListIframe[0].ID);
            }
          }
      }, reject => {
          
          this.errors = reject.error.error;
      });
    }
  }

  bindStateList() {
    this.dataService.post('getStates', {})
      .subscribe(setting => {
        if ( setting.success ) {
          this.stateList = setting.data.states;
        }
      }, reject => {
          
        this.errors = reject.error.error;
      });
  }

  loadCharts(){
    // Chart - Contracts By Terms
    this.chartContractsByTerms();
    // Chart - Contracts By Status
    this.chartContractsByStatus();
    // Chart - Contracts By Product
    this.chartContractsByProducts();
    // Chart - Contracts By Referrals
    this.chartContractsByReferrals();
    // Chart - Contracts By Age
    this.chartContractsByAge();
    // Chart - Contracts By Customer Location
    this.chartContractsByLocation();
    // Chart - Number of Purchased Contracts (Top 5 dealers)
    this.chartContractsByPurchased();
    // Chart - Total of Adjested Amount (Top 5 dealers)
    this.chartContractsByPurchasedAmount();
    // Chart - Number of Generated Contracts (Top 5 dealers)
    this.chartContractsByGenerated();
    // Chart - Total of Adjested Amount (Top 5 dealers)
    this.chartContractsByGeneratedAmount();
  }

  showCustom(val) {
    this.where['filertype'] = 'custom';
    this.showCustomFiler = val;
  }

  contractPaymentChange(value){
    let contract = {};
    if ( value.type == "submit" && !this.isSubmitRun ) {
        this.isSubmitRun = true;
        contract = value.data;
        //contract['term'] = null;
        contract['saveForLater'] = true;
        if ( this.currentUser['user_role'] == 'superadmin' || this.currentUser['user_role'] == 'representative' ) {
          contract['provider_id'] = value.data.provider_id;
        } else {
          contract['provider_id'] = this.contractToCal.provider_id;
        }

        if ( contract['property_type'] != undefined ) {
          contract['property'] = 'new';
        }

        this.dataService.post('contracts', contract)
          .subscribe(template => {
              if ( template.success ) {
                // this.isSubmitRun = false;
                this.alertService.success(template.message, true);

                // Socket Trigger
                this.notificationService.sendEmitter('notifyToAll', {mode: 'contract-init'});

                //this.router.navigate(['/contracts/']);
                this.router.navigate(['/contracts/edit/'+template.data.last_id]);
              } else {
                this.isSubmitRun = false;
                this.alertService.success(template.message, true);
              }
          }, reject => {
            this.isSubmitRun = false;
            this.errors = reject.error.error;
          });
    }
  }

  showTermSectionParent(val = true){
    this.showTermSection = val;
  }

  applyChartFilter(filertype){
    this.errors = {};
    this.showCustomFiler = false;
    this.where['filertype'] = filertype;
    //console.log(this.where);
    this.loadCharts();
  }

  submitCustomFilter() {
    
    this.errors = {};
    if ( $('#daterange').val() == '' || $('#daterange').val() == undefined ) {
      this.errors.daterange = '1';
      return false;
    }

    //console.log(this.where);
    this.loadCharts();
  }

  selectDate(value: any){
    this.where['startdate'] = value.start.format("MM/DD/YYYY");
    this.where['enddate'] = value.end.format("MM/DD/YYYY");
  }

  contractsUpdate(f){}

  chartContractsByTerms(){

    this.contractsByTermsLoder = true;
    this.dataService.post('contractsByTerms', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            this.contractsByTerms = {
                labels: chart.data.term,
                datasets: [
                    {
                        label: 'No of Contracts',
                        backgroundColor: this.currentUser['theme_color'],
                        borderColor: this.currentUser['theme_color'],
                        data: chart.data.total
                    }
                ]
            }
          }
          this.contractsByTermsLoder = false;
      }, reject => {
        this.contractsByTermsLoder = false;
      });
  }

  chartContractsByPurchased(){

    this.contractsByPurchasedLoder = true;
    this.dataService.post('contractsByPurchased', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            this.contractsByPurchased = {
                labels: chart.data.dealer,
                datasets: [
                    {
                        label: 'No of Contracts',
                        backgroundColor: this.currentUser['theme_color'],
                        borderColor: this.currentUser['theme_color'],
                        data: chart.data.total
                    }
                ]
            }
          }
          this.contractsByPurchasedLoder = false;
      }, reject => {
        this.contractsByPurchasedLoder = false;
      });
  }

  chartContractsByGenerated(){

    this.contractsByGeneratedLoder = true;
    this.dataService.post('contractsByGenerated', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            this.contractsByGenerated = {
                labels: chart.data.dealer,
                datasets: [
                    {
                        label: 'No of Contracts',
                        backgroundColor: this.currentUser['theme_color'],
                        borderColor: this.currentUser['theme_color'],
                        data: chart.data.total
                    }
                ]
            }
          }
          this.contractsByGeneratedLoder = false;
      }, reject => {
        this.contractsByGeneratedLoder = false;
      });
  }

  chartContractsByPurchasedAmount(){

    this.contractsByPurchasedAmountLoder = true;
    this.dataService.post('contractsByPurchasedAmount', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            this.contractsByPurchasedAmount = {
                labels: chart.data.dealer,
                datasets: [
                    {
                        label: 'Total Adjusted Cash',
                        backgroundColor: this.currentUser['theme_color'],
                        borderColor: this.currentUser['theme_color'],
                        data: chart.data.total
                    }
                ]
            }
          }
          this.contractsByPurchasedAmountLoder = false;
      }, reject => {
        this.contractsByPurchasedAmountLoder = false;
      });
  }

  chartContractsByGeneratedAmount(){

    this.contractsByGeneratedAmountLoder = true;
    this.dataService.post('contractsByGeneratedAmount', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            this.contractsByGeneratedAmount = {
                labels: chart.data.dealer,
                datasets: [
                    {
                        label: 'Total Adjusted Cash',
                        backgroundColor: this.currentUser['theme_color'],
                        borderColor: this.currentUser['theme_color'],
                        data: chart.data.total
                    }
                ]
            }
          }
          this.contractsByGeneratedAmountLoder = false;
      }, reject => {
        this.contractsByGeneratedAmountLoder = false;
      });
  }

  chartContractsByStatus(){

    this.contractsByStatusLoder = true;
    this.dataService.post('contractsByStatus', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            /*this.contractsByStatus = {
            labels: ['Incomplete', 'Inapproval', 'Approved', 'Purchased', 'Abandoned'],
            datasets: [
                {
                    data: chart.data.total,
                    backgroundColor: [
                        "#FA7E57",
                        "#f39c11",
                        "#43a047",
                        "#ef5350",
                        "#878787"
                    ],
                    hoverBackgroundColor: [
                        "#FA7E57",
                        "#f39c11",
                        "#43a047",
                        "#ef5350",
                        "#878787"
                    ]
                }]    
            };*/

            this.contractsByStatus = {
                labels: chart.data.status,
                datasets: [
                    {
                        label: 'No of Contracts',
                        backgroundColor: this.currentUser['theme_color'],
                        borderColor: this.currentUser['theme_color'],
                        data: chart.data.total
                    }
                ]
            }
          }
          this.contractsByStatusLoder = false;
      }, reject => {
        this.contractsByTermsLoder = false;
      });
  }
  
  chartContractsByProducts(){

    this.contractsByProductsLoder = true;
    this.dataService.post('contractsByProducts', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            this.contractsByProducts = {
              labels: chart.data.products,
              datasets: [
                {
                      data: chart.data.total,
                      backgroundColor: [
                        this.currentUser['theme_color'],
                        this.currentUser['theme_color']+'7a',
                        this.currentUser['theme_color']+'47'
                      ],
                      hoverBackgroundColor: [
                        this.currentUser['theme_color'],
                        this.currentUser['theme_color']+'7a',
                        this.currentUser['theme_color']+'47'
                      ]
                }]    
            };

          }
          this.contractsByProductsLoder = false;
      }, reject => {
        this.contractsByProductsLoder = false;
      });
  }

  chartContractsByReferrals(){

    this.contractsByReferralsLoder = true;
    this.dataService.post('contractsByReferrals', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            this.contractsByReferrals = {
              labels: chart.data.referrals,
                datasets: [
                  {
                      label: 'Contract Generate',
                      backgroundColor: this.currentUser['theme_color']+'61',
                      borderColor: this.currentUser['theme_color'],
                      pointBackgroundColor: this.currentUser['theme_color'],
                      pointBorderColor: '#fff',
                      pointHoverBackgroundColor: '#fff',
                      pointHoverBorderColor: this.currentUser['theme_color'],
                      data: chart.data.total
                  }
                ]
            };

          }
          this.contractsByReferralsLoder = false;
      }, reject => {
        this.contractsByReferralsLoder = false;
      });
  }

  chartContractsByAge(){

    this.contractsByAgeLoder = true;
    this.dataService.post('contractsByAge', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {

            this.contractsByAge = {
              labels: chart.data.age,
              datasets: [
                {
                  data: chart.data.total,
                  backgroundColor: [
                    this.currentUser['theme_color'],
                    this.currentUser['theme_color']+'7a',
                    this.currentUser['theme_color']+'47',
                    '#b9b7b747',
                    '#b9b7b77a',
                  ],
                  hoverBackgroundColor: [
                    this.currentUser['theme_color'],
                    this.currentUser['theme_color']+'7a',
                    this.currentUser['theme_color']+'47',
                    '#b9b7b747',
                    '#b9b7b77a',
                  ]
                }]    
            };

          }
          this.contractsByAgeLoder = false;
      }, reject => {
        this.contractsByAgeLoder = false;
      });
  }

  chartContractsByLocation(){

    this.contractsByLocationLoder = true;
    this.dataService.post('contractsByLocation', {'where': this.where})
      .subscribe(chart => {
          this.contractsByLocationLoder = false;
          if ( chart.success ) {
            
            /*this.contractsByLocation = {
              labels: chart.data.state,
              datasets: [
                  {
                      label: 'No of Contracts',
                      backgroundColor: this.currentUser['theme_color'],
                      borderColor: this.currentUser['theme_color'],
                      data: chart.data.total
                  }
              ]
            }*/

            this.use_map_data = new Datamap({
              scope: 'usa',
              element: document.getElementById('map_election'),
              geographyConfig: {
                highlightBorderColor: '#ccc',
                popupTemplate: function(geography, data) {
                  return '<div class="hoverinfo">' + geography.properties.name + ' Contracts:' +  data.electoralVotes + ' '
                },
                highlightBorderWidth: 3
              },
              fills: {
                'haveContract': this.currentUser['theme_color'],
                defaultFill: this.currentUser['theme_color']+'47'
              },
              data: chart.data.state
            });
            this.use_map_data.labels();

          }
          
      }, reject => {
        this.contractsByLocationLoder = false;
      });
  }

  exportChart(){
    this.exporting = true;
    this.dataService.post('exportChart', {'where': this.where})
      .subscribe(chart => {
          if ( chart.success ) {
            window.location.href = chart.data;
          }
          this.exporting = false;
      }, reject => {
        this.exporting = false;
      });
  }

  generateIframe(form: NgForm) {
    
    this.errors = {};
    this.iframe.code = '';
    this.loading = true;

    if ( this.currentUser['user_role'] == 'admin' ) {
      form.value.provider_id = this.iframe.provider_id;
    }
    form.value.logo_url = '';
    if (this.attachment64Data !== undefined && this.attachment64Data !== '' && this.attachmentImageName !== ''){
        form.value.logo_url = this.attachment64Data;
        form.value.logo_name = this.attachmentImageName;
        form.value.logo_docs = this.attachmentImageType;
    }
    if ( this.iframe.type == 'free_instant_quote' ) {
      form.value.product_id = this.iframe.product_id;
    }
    console.log(form.value)
    
    this.dataService.post('generateIframes', form.value)
        .subscribe(ifr => {
            if ( ifr.success ) {
              this.iframe.code = ifr.data.iframe_url;
              if ( form.value.type == 'external_checkout' ) {
                this.itemNew = ifr.data.iframe_url;
              } else {
                this.itemNew = '<iframe width="100%" src="'+ifr.data.iframe_url+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
              }
            }
            this.loading = false;
      }, reject => {
          console.log(reject);
          this.loading = false;
      });
  }

  downloadContract(form: NgForm) {

    this.errors = {};
    this.loadingDown = true;

    if ( this.currentUser['user_role'] != 'superadmin' && this.currentUser['user_role'] != 'representative' ) {
      form.value.provider_id = this.contractDownload.provider_id;
    }

    this.dataService.post('contracts/download-blank', form.value)
        .subscribe(con => {
            if ( con.success ) {
              window.location.href = con.data;
            }
            this.loadingDown = false;
      }, reject => {
          console.log(reject);
          this.loadingDown = false;
      });
  }

  onOpenCompany(event, mode){
    if ( mode == 'company' ) {
       
      if ( this.iframe.provider_id != undefined && this.iframe.provider_id != '' ) {
        this.dataService.post('getCompanies', {'provider_id': this.iframe.provider_id})
           .subscribe(company => {
             if ( company.success ) {
               this.companyList = company.data.companies;
             }
         }, reject => {
             console.log(reject);
         });
      }

     }
  }

  onOpen(event, mode) {

    if ( mode == 'provider' ) {
       
       this.dataService.post('getProviders', {'provider_type': 'rto'})
          .subscribe(provider => {
              if ( provider.success ) {
                this.providerList = provider.data.providers;
              }
        }, reject => {
            console.log(reject);
        }); 

    } else if ( mode == 'company' ) {
       
      if ( this.contractDownload.provider_id != undefined && this.contractDownload.provider_id != '' ) {
        this.dataService.post('getCompanies', {'provider_id': this.contractDownload.provider_id})
           .subscribe(company => {
             if ( company.success ) {
               this.companyList = company.data.companies;
             }
         }, reject => {
             console.log(reject);
         });
      }

     } else if ( mode == 'dealer' ) {
        
        if ( this.iframe.provider_id != undefined && this.iframe.provider_id != '' ) {
          let where = {'user_role': ['dealer', 'company_admin'], 'provider_id': this.iframe.provider_id, 'company_id': this.iframe.company_id};
          this.dataService.post('getUsers', {'where': where})
            .subscribe(user => {
                if ( user.success ) {
                  this.dealerList = user.data.users;
                }
          }, reject => {
              console.log(reject);
          });
        }
    } else if ( mode == 'fonts' ) {
        this.dataService.post('getFonts')
          .subscribe(fontData => {
              if ( fontData.success ) {
                this.fontList = fontData.data;
              }
        }, reject => {
            console.log(reject);
        }); 
    }
  }

  onItemSelect(item, mode) {
      if ( mode == 'provider' ) {
      
        this.iframe.provider_id = item.id;
        this.iframe.company_id = null;
        this.iframe.c_user_id = null;
        this.getProductsIframe();
      } else if ( mode == 'providerConDown' ) {
        this.getProducts();
      } else if ( mode == 'company' ) {
        this.iframe.company_id = item.id;
        this.iframe.c_user_id = null;
        this.getProductsIframe();
      } else if ( mode == 'dealer' ) {
        this.iframe.c_user_id = item.id;
        this.getProductsIframe();
      }
    this.iframe.code = '';
    this.copyToTxt = 'Copy Code';
  }

  // Copy text
copyToClipboard(iframe_url) {
  let item = '';
    
  if ( this.iframe.type === 'external_checkout' ) {
    item = iframe_url;
  } else {
    item = '<iframe width="100%" src="'+iframe_url+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
  }
      
  document.addEventListener('copy', (e: ClipboardEvent) => {
  e.clipboardData.setData('text/plain', (item));
  e.preventDefault();
  document.removeEventListener('copy', null);
  });
  document.execCommand('copy');
  this.copyToTxt = 'Copied!';
  }

  readUploadLogoUrl(event:any) {
    if (event.target.files && event.target.files[0]) {
      
      $('#previewLabel').html(event.target.files[0].name); 
      if ( event.target.files[0].type !== 'image/jpeg' && event.target.files[0].type !== 'image/jpg' && event.target.files[0].type !== 'image/png' && event.target.files[0].type !== 'image/gif') {
        this.removeUploadLogoUrl();
        this.alertService.error('Please select valid file', true);
        return false;
      }
      const reader = new FileReader();
      reader.onload = (event: ProgressEvent) => {
        this.attachment64Data = (<FileReader>event.target).result; 
        this.attachmentImage = (<FileReader>event.target).result;
        this.previewImage = true;
      }
      
      this.attachmentImageName = event.target.files[0].name;
      this.attachmentImageType = event.target.files[0].type;
      reader.readAsDataURL(event.target.files[0]);
    }
  }

   removeUploadLogoUrl() {
      $('#previewImage').val('');
      $('#previewLabel').html('Please select logo');
      this.attachmentImage =  this.attachmentImageName = null;
      this.previewImage = false;
      this.attachment64Data = '';
    }
  }
