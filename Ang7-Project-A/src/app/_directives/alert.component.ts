﻿import { Component, OnInit } from '@angular/core';
import { AlertService } from '../_services/index';

@Component({
    moduleId: module.id.toString(),
    selector: 'alert',
    templateUrl: 'alert.component.html'
})

export class AlertComponent implements OnInit {
    message: any;
    status: any;
    showHide: boolean;
    showHideCon: boolean;
    showHideDoubleCon: boolean;
    
    constructor(private alertService: AlertService) {
      this.showHide = true;
      this.showHideCon = true;
      this.showHideDoubleCon = true;
    }

    ngOnInit() {
        this.alertService.getMessage().subscribe(message => { 
            if ( message != undefined ) {
                this.message = message;
                this.showHide = true;
                this.showHideCon = true;
                this.showHideDoubleCon = true;
                if ( this.message.type != 'conform' && this.message.type != 'confirm' && this.message.type != 'doubleconfirm' ) {
                    setTimeout(() => { 
                        this.showHide = false; 
                    }, 5000);
                }
            }
        });
    }

    removeAlert() {
      this.showHide = !this.showHide;
      this.showHideCon = !this.showHideCon;
      this.showHideDoubleCon = !this.showHideDoubleCon;
    }
}