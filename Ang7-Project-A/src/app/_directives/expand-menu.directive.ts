import { Directive, EventEmitter, ElementRef, HostBinding, HostListener, Output, AfterViewChecked, Input, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppGlobals } from '../app.global';
import * as _ from "lodash";

@Directive({
  selector:'[expandMenu]'
})

export class ExpandMenu {

  @HostBinding('class.open') isOpen = false;
  @HostBinding('class.popup') popup = false;
  @HostListener('click') toggleOpen($event){
    this.isOpen = !this.isOpen;
    if ( !this.isOpen ) {
      this.popup = false;
    }
  }
  
  constructor(private elementRef: ElementRef) {}
  
  @Output() clickElsewhere = new EventEmitter<MouseEvent>();
  @HostListener('document:click', ['$event'])
  
  public onDocumentClick(event: MouseEvent): void {
    const targetElement = event.target as HTMLElement;
    
    let targetElementTotal = $(targetElement).offset().top;
    let popoverBoxHeight = $(targetElement).next('.popover-box').outerHeight();
    let mainContainerHeight = $( window ).height();
    if ( ((targetElementTotal +  popoverBoxHeight + 45) - $(window).scrollTop()) > mainContainerHeight ) {
      // this.popup = true;
      this.popup = false;
    } else {
      this.popup = false;
    }
    
    /*let targetElementTotal = $(targetElement).offset().top + $(targetElement).outerHeight();
    let popoverBoxHeight = $(targetElement).next('.popover-box').outerHeight();
    let mainContainerHeight = $('.main-container').height();
    if ( (targetElementTotal +  popoverBoxHeight) > mainContainerHeight ) {
      this.popup = true;
    } else {
      this.popup = false;
    }*/

    if ( $(targetElement).next('.popover-box').data('direction') != undefined ) {
      if ( $(targetElement).next('.popover-box').data('direction') == 'top' ) {
        this.popup = true;
      } else {
        this.popup = false;
      }
    }

    // Check if the click was outside the element
    if (targetElement && !this.elementRef.nativeElement.contains(targetElement) ) {
      this.isOpen = false;
      this.popup = false;
      this.clickElsewhere.emit(event);
    }
  }
}

@Directive({ 
  selector: '[preferencesList]'
})
export class PreferencesList {

    list:any;
    listHtml:any;
    currentUser:any;
    
    constructor(private el: ElementRef, private _global: AppGlobals, private renderer: Renderer2, private router: Router) { }

    ngOnInit() {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.preferencesNavList();
      this.generateHtml();
      this.el.nativeElement.innerHtml = this.listHtml;
    }

    preferencesNavList(){
      if (this.currentUser.provider_type == 'rto') {
            this.list = [
                {'id': '/preferences/account-defaults', 'itemName': 'Account Defaults'},
                {'id': '/preferences/contracts-options/terms-settings', 'itemName': 'Terms Settings'},
                {'id': '/preferences/contracts-options/contracts-templates', 'itemName': 'Contracts Templates'},
                {'id': '/preferences/email-options/email-templates', 'itemName': 'Email Templates'},
                {'id': '/preferences/add-ons', 'itemName': 'Add Ons'},
                {'id': '/preferences/import', 'itemName': 'Import'},
                {'id': '/preferences/export', 'itemName': 'Export'},
                {'id': '/preferences/lead-form', 'itemName': 'Lead Form'},
                {'id': '/preferences/under-maintenance', 'itemName': 'Under Maintenance Mode'},
                {'id': '/preferences/notification', 'itemName': 'Notification'}
            ];

            if ( this.currentUser.user_role != 'admin' && this.currentUser.user_role != 'superadmin' ) {
                if ( this.currentUser.user_role == 'dealer' ) {
                    this.list.splice(1, 4);
                    this.list.splice(1, 2);
                    this.list.splice(2, 1);
                } else if (this.currentUser.user_role == 'representative') {
                    this.list.splice(1, 4);
                    this.list.splice(3, 2);
                } else {
                    this.list.splice(1, 8);
                }
            } else if ( this.currentUser.user_role == 'admin' ) {
                this.list.splice(3, 1);
                this.list.splice(7, 1);
                this.list.splice(6, 1);
            } else if ( this.currentUser.user_role == 'superadmin' ) {
                this.list.splice(7, 1);
            }
            

        } else if (this.currentUser.provider_type == 'securestay') {
            this.list = [
                {'id': '/preferences/account-defaults', 'itemName': 'Account Defaults'},
                {'id': '/preferences/agreement-settings', 'itemName': 'Agreement Settings'},
                {'id': '/preferences/sms-templates', 'itemName': 'SMS Templates'},
                {'id': '/preferences/contracts-templates', 'itemName': 'Agreement Templates'},
                {'id': '/preferences/add-ons', 'itemName': 'Add Ons'},
                {'id': '/preferences/notification', 'itemName': 'Notification'}
              ];

            if (this.currentUser.user_role == 'representative') {
              this.list.splice(1, 4);
            }
      }
    }

    generateHtml(){

      let html = '<button class="btn btn-secondary"><i class="fas fa-bars"></i></button>';
      html += '<input class="popover-checkbox" type="checkbox" #navBar>';
      html += '<div class="popover-box">';
        html += '<ul class="action-list">';

          let that = this;
          _.forEach(this.list, function(item) {
            html += '<li class="'+((that.router.url == item.id) ? 'active' : '')+'" routerLink="'+item.id+'">'+item.itemName+'</li>';
          });
        html += '</ul>';
      html += '</div>';

      this.listHtml = html;
      this.renderer.setProperty(this.el.nativeElement, 'innerHTML', this.listHtml);
    }

    @HostListener('click', ['$event.target']) onClick($event){
      var goRoute = $event.getAttribute('routerLink');
      if ( goRoute != null ) {
        this.router.navigate([ goRoute ]);
      }
    }
}

@Directive({ 
  selector: '[companyList]'
})
export class CompanyList {

    list:any;
    listHtml:any;
    currentUser:any;
    isProviderMode: boolean = false;
    provider_id = '';
    company_id = '';
    
    constructor(private el: ElementRef, private _global: AppGlobals, private renderer: Renderer2, private router: Router, private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

      // check current route is provider route or not
      if ( this.router.url.indexOf('providers/') !== -1 ) {
        this.isProviderMode = true;
        this.activatedRoute.params.subscribe((params: Params) => {
          this.provider_id = params.id;
        });
      }

      this.activatedRoute.params.subscribe((params: Params) => {
        this.company_id = params.comid;
      });

      this.companyNavList();
      this.generateHtml();
      this.el.nativeElement.innerHtml = this.listHtml;
    }

    routerPrefix() {
      var prefix = '';
      
      if ( this.isProviderMode ) {
        prefix = '/providers/' + this.provider_id + '/companies/'+this.company_id;
      } else {
        prefix = '/companies/' + this.company_id;
      }

      return prefix;
    }

    companyNavList(){
      this.list = [
        {'id': this.routerPrefix() + '/statistics', 'itemName': 'Statistics'},
        {'id': this.routerPrefix() + '/contracts', 'itemName': 'Contracts'},
        {'id': this.routerPrefix() + '/users', 'itemName': 'Users'},
        {'id': this.routerPrefix() + '/account-defaults', 'itemName': 'Account Defaults'},
        {'id': this.routerPrefix() + '/contracts-options/terms-settings', 'itemName': 'Products & Terms Settings'},
        {'id': this.routerPrefix() + '/notification', 'itemName': 'Notification'},
      ];
    }

    generateHtml(){

      let html = '<button class="btn btn-secondary"><i class="fas fa-bars"></i></button>';
      html += '<input class="popover-checkbox" type="checkbox" #navBar>';
      html += '<div class="popover-box">';
        html += '<ul class="action-list">';

          let that = this;
          _.forEach(this.list, function(item) {
            html += '<li class="'+((that.router.url == item.id) ? 'active' : '')+'" routerLink="'+item.id+'">'+item.itemName+'</li>';
          });
        html += '</ul>';
      html += '</div>';

      this.listHtml = html;
      this.renderer.setProperty(this.el.nativeElement, 'innerHTML', this.listHtml);
    }

    @HostListener('click', ['$event.target']) onClick($event){
      var goRoute = $event.getAttribute('routerLink');
      if ( goRoute != null ) {
        this.router.navigate([ goRoute ]);
      }
    }
}