import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // add authorization header with jwt token if available
        let providerCode:any = 1;
        //let companyCode:any = '';
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        
        if ( currentUser != null ) {
            providerCode = currentUser.provider_id;
        }

        // overwrite provider code
        if ( this.router.url.indexOf('providers/') !== -1 ) {
            let urlParam =  this.router.url.split("/");
            providerCode = urlParam['2'];
        }

        /*if ( currentUser != null ) {
            if ( currentUser.company_id != null ) {
                companyCode = currentUser.company_id;
            }
        }*/

        // overwrite company code
        /*if ( this.router.url.indexOf('companies/') !== -1 ) {
            let urlParam =  this.router.url.split("/");
            companyCode = urlParam['2'];
        }*/
        
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    'Accept': 'application/json, text/plain, * / *',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Authorization': `Bearer ${currentUser.token}`,
                    'Provider-Code': `${providerCode}`,
                    'X-Timezone-Offset': this.getTimezoneOffset()
                    //'Company-Code': `${companyCode}`,
                }
            });
        }
        return next.handle(request);
    }

    private getTimezoneOffset() : string {
        // return Intl.DateTimeFormat().resolvedOptions().timeZone;
		return( String( new Date().getTimezoneOffset()*60 ) );
	}
}
