import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../_guards/index';
import { UsersComponent } from './users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';

const routes: Routes = [
	{ 
		path: '', 
		component: UsersComponent, 
		canActivate: [AuthGuard],
		data: { baiPass: ['superadmin', 'admin', 'representative', 'company_admin'], permissions: { users: ['view', 'add', 'edit', 'delete'] } }
	},
	{ 
    path: 'detail/:uid', 
    component: UserDetailComponent, 
    canActivate: [AuthGuard],
    data: { baiPass: ['superadmin', 'admin', 'representative', 'company_admin'], permissions: { users: ['view'] } }
  },
];

@NgModule({
  imports: [
  	RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
