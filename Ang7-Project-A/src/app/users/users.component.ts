import { Component, OnInit, ViewChild, ElementRef, HostListener, Inject } from '@angular/core';
import {User} from '../_models';
import {UserService} from '../_services';
import { AlertService, AuthenticationService, DataService, NotificationService, RingcentralService } from '../_services/index';
import { AppGlobals } from '../app.global';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { environment } from './../../environments/environment';
import { timeInterval } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: []
})
export class UsersComponent implements OnInit {
  
  entitySingular = 'Profile & Preference';
  entityPlural = 'Profile & Preferences';
  entityApiPrefix = 'users';
  
  provider: { [index:string] : any } = {}
  imageurl:any;
  show = false;
  
  userFilter = [
    // {'id': 'agent', 'text': 'Agent', 'itemName': 'Agent'},
    {'id': 'admin', 'text': 'Provider Admin', 'itemName': 'Provider Admin'},
    {'id': 'company_admin', 'text': 'Company Admin', 'itemName': 'Company Admin'},
    {'id': 'dealer', 'text': 'Dealer', 'itemName': 'Dealer'},
    {'id': 'subdealer', 'text': 'Subdealer', 'itemName': 'Subdealer'},
    {'id': 'investor', 'text': 'Investor', 'itemName': 'Investor'},
    {'id': 'accountant', 'text': 'Accountant', 'itemName': 'Accountant'},
    {'id': 'customer', 'text': 'Customer', 'itemName': 'Customer'},
  ];

  userBulkAction = [
    {'id': 'active', 'text': 'Active'},
    {'id': 'inactive', 'text': 'Inactive'},
    //{'id': 'delete', 'text': 'Delete'},
  ];

  statusFilter = [
    {'id': 'active', 'text': 'Active', 'itemName': 'Active'},
    {'id': 'inactive', 'text': 'In Active', 'itemName': 'In Active'}
  ];

  currentUser: User;
  users: User[] = [];
  userCount = [];
  selectedUsers = [];
  where = {'role': null, 'string': ''};
  sort = {
    'first_name': '',
    'email_id': '',
    'contact_no': '',
    'user_id': 'desc',
  };

  selectedAll: any;
  selected: any;
  activeMenu:string = null;

  selector: string = '.main-panel';
  perPageRecords = this._global.perPageRecords;
  totalRecords = 0;
  totalPages = 0;
  currentPage = 1;
  
  @ViewChild(InfiniteScrollDirective) infiniteScroll: InfiniteScrollDirective;

  // data to pass change password side window
  changePasswordSide = false;
  deleteSide = false;
  userData = [];

  // data to pass create user side window
  createUserSide = false;

  hideProviderCol: boolean = false;
  hideCompanyCol: boolean = false;
  isProviderMode: boolean = false;
  isCompanyMode: boolean = false;
  currentRole = '';

  providerList = [];
  selectedProvider;
  selectedProviderModel;
  /*providerDropSettings = {
    text: "Search By Providers",
    badgeShowLimit: 1,
    enableSearchFilter: true,
    enableCheckAll: false,
    classes: "select-custom filter-div", //custom-div
  };*/
  
  companyList = [];
  selectedCompany;
  selectedCompanyModel;

  selectedRole;
  selectedRoleModel;
  /*roleDropSettings = {
    text: "Search By Role",
    singleSelection: true, 
    enableSearchFilter: true,
    enableCheckAll: false,
    classes: "select-custom filter-div", //custom-div
    primaryKey: 'id',
    labelKey: 'text'
  };*/

  selectedStatus;
  selectedStatusModel;
  /*statusDropSettings = {
    text: "Search By Status",
    singleSelection: true, 
    enableSearchFilter: true,
    enableCheckAll: false,
    classes: "select-custom filter-div", //custom-div
    primaryKey: 'id',
    labelKey: 'text'
  };*/

  busy = false;
  company_id;
  provider_id;
  filterSection : { [index:string] : any } = {};
  filteredItem = [];
  copiedItem:any;
  @ViewChild("filterCheckbox") filterCheckbox: ElementRef;
  @ViewChild("actionCheckbox") actionCheckbox: ElementRef;

  deviceType = '';
  toggleRow: { [index:string] : any } = {};
  calling: boolean = false;

  isRingCentralRegistered = false;
  
  constructor(private deviceService: DeviceDetectorService, private _eref: ElementRef, private activatedRoute: ActivatedRoute, private userService: UserService, private alertService: AlertService, private dataService: DataService, public _global: AppGlobals, private router: Router, private notificationService: NotificationService, public ringCentralService: RingcentralService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.busy = true;
    this.imageurl = environment.uploadUrl;
    this.getProfileDetails().then((res) => {
      if ( this.provider.type === 'securestay' ) {
        this.userFilter = [
          //{'id': 'representative', 'text': 'Representative', 'itemName': 'Representative'},
          {'id': 'admin', 'text': 'Provider Admin', 'itemName': 'Provider Admin'},
        ];
      }
      this.ngOnInitSub();
    });
  }

  ngOnInitSub() {
    this.busy = true;

    this.deviceType = (this.deviceService.isDesktop() ? 'desktop' : (this.deviceService.isMobile() ? 'mobile' : (this.deviceService.isTablet() ? 'tablet' : '')));
    
    // check current route is provider route or not
    if ( this.router.url.indexOf('providers/') !== -1 ) {
        this.isProviderMode = true;
        this.hideProviderCol = true;
        this.activatedRoute.params.subscribe((params: Params) => {
          this.where['provider_id'] = params.id;
          this.provider_id = params.id;
        });
    }

    // check current route is company route or not
    if ( this.router.url.indexOf('companies/') !== -1 ) {
      this.activatedRoute.params.subscribe((params: Params) => {
        this.isCompanyMode = true;
        this.hideCompanyCol = true;
        this.where['company_id'] = params.comid;
        this.company_id = params.comid;
      });
    }

    this.currentRole = this._global.getlocalStorage('user_role');
    let roleFlag = false;
    if ( this.currentRole == 'superadmin' ) {
        this.userFilter.unshift({'id': 'representative', 'text': 'Representative', 'itemName': 'Representative'});
        this.userFilter.unshift({'id': 'agent', 'text': 'Agent', 'itemName': 'Agent'});
    }
    if ( this.currentRole == 'superadmin' && this.isProviderMode) {
        delete this.userFilter[0];
        this.userFilter.splice(0, 2);
        roleFlag = true;
    }
    if ( this.currentRole != 'superadmin' && this.currentRole != 'representative' ) {
        roleFlag = true;
        this.hideProviderCol = true;
       // this.userFilter.splice(0, 1);
        this.where['provider_id'] = this._global.getlocalStorage('provider_id');
    }
    if ( this.currentRole != 'superadmin' && this.currentRole != 'representative' && this.currentRole != 'admin' ) {
        if(!roleFlag){
          delete this.userFilter[0];
          roleFlag = true;
        }
        this.hideCompanyCol = true;
    }
    if(this.provider.type == 'securestay'){
      this.hideCompanyCol = true;
    }
    if ( this.currentRole != 'superadmin' && this.currentRole != 'representative' && this.currentRole != 'admin' && this.currentRole != 'company_admin' ) {
        if(!roleFlag){
          delete this.userFilter[0];
          this.userFilter.splice(0, 1);
          roleFlag = true;
        }  
    }
    
    if ( this.isCompanyMode || this.currentRole == 'company_admin' ) {
        this.userFilter.splice(0, 1);
        this.userFilter.splice(3, 1);
        this.userFilter.splice(4, 1);
    }
    
    // if ( this.isCompanyMode && this.currentRole == 'admin' ) {
    //     this.userFilter.splice(0, 2);
    //     this.userFilter.splice(3, 1);
    //     this.userFilter.splice(4, 1);
    // } 
    this.getListData(true);
  }

  ngAfterViewInit() {
    this.syncNavHeight();
  }

  syncNavHeight() {
    //let mainCont = $('.main-container').offset().top;
    let mainCont = ($('header').outerHeight() + $('.page-header').outerHeight());
    let secondaryHeight = $('.secondary-tab').outerHeight();
    this._eref.nativeElement.querySelector('.secondary-tab').style['top'] = mainCont + 'px';
    if ( this._eref.nativeElement.querySelector('.table-part thead') != null ) {
      if ( this.deviceType == 'mobile' ) {
        this._eref.nativeElement.querySelector('.table-part thead').style['top'] = mainCont + 'px';
      } else {
        this._eref.nativeElement.querySelector('.table-part thead').style['top'] = (mainCont + secondaryHeight) + 'px';
      }
    }
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
     if (window.pageYOffset > 30) {
       let filterBar = document.getElementById('secondary-tab');
       filterBar.classList.add('sticky-header');

       let tableDiv = document.getElementById('main-panel');
       tableDiv.classList.add('sticky-table-part');

     } else {
        let filterBar = document.getElementById('secondary-tab');
        filterBar.classList.remove('sticky-header'); 

        let tableDiv = document.getElementById('main-panel');
        tableDiv.classList.remove('sticky-table-part');
     }
  }

  getProfileDetails(){
    return new Promise((resolve, reject) => {
      this.dataService.get('providerProfile')
        .subscribe(provider => {
            if ( provider.success ) {
              this.provider = provider.data;
            }
            resolve();
        }, reject => {
          console.log(reject);
          resolve();
        });
    });
  }

  routerPrefix(){
    var prefix = '';
    
    if ( this.isProviderMode && this.isCompanyMode ) {
      prefix = '/providers/' + this.provider_id + '/companies/' + this.company_id;
    } else if ( this.isProviderMode ) {
      prefix = '/providers/' + this.provider_id;
    } else if ( this.isCompanyMode ) {
      prefix = '/companies/' + this.company_id;
    }

    return prefix;
  }

  onClick(event) {
   if (!this._eref.nativeElement.contains(event.target)) // or some similar check
      if ( this.actionCheckbox.nativeElement.checked ) {
       this.actionCheckbox.nativeElement.click();
      }
  }

  quickSearch (value) {
    this.getListData(true);
  }

  toggle() {
    this.show = !this.show;
  }

  selectDrop(val, mode){
    if ( mode == 'provider' ) {
      this.selectedProvider = val;
    } else if ( mode == 'company' ) {
      this.selectedCompany = val;
    } else if ( mode == 'status' ) {
      this.selectedStatus = [val];
    } else if ( mode == 'role' ) {
      this.selectedRole = [val];
    }
  }

  onOpen(event, mode) {

    if ( mode == 'provider' ) {
       
       this.dataService.post('getProviders', {'provider_type': 'rto'})
          .subscribe(provider => {
              if ( provider.success ) {
                this.providerList = provider.data.providers;
              }
        }, reject => {
            console.log(reject);
        }); 
    } else if ( mode == 'company' ) {
       
        let dataPass = {};
        if ( this.isProviderMode ) {
          dataPass = {'provider_id': this.provider_id};
        }

        if ( this.currentRole != 'superadmin' && this.currentRole != 'representative' ) {
          dataPass = {'provider_id': this.where['provider_id']};
        }

        if ( this.selectedProvider != undefined ) {
          dataPass['provider_filter'] =  this.selectedProvider;
        }

        this.dataService.post('getCompanies', dataPass)
            .subscribe(company => {
              if ( company.success ) {
                this.companyList = company.data.companies;
              }
          }, reject => {
              console.log(reject);
          });
    }
  }

  /*onItemSelect(item, mode){

    if ( mode == 'provider' ) {

      this.where['provider_id'] = this.selectedProvider;
      this.getListData(true);

    }
  }*/

  onItemSelect(mode){

    this.filterCheckbox.nativeElement.click();
    
    const index = this.filteredItem.findIndex((item) => {
      return item.mode == mode;
    });

    if ( mode == 'provider' ) {

      this.where['provider_id'] = this.selectedProvider;
      this.copiedItem = Object.assign([], [] , this.selectedProvider );
      if (!(index !== -1)) {
        this.filteredItem.push({'mode': mode, 'data': this.copiedItem});
      } else {
        this.filteredItem[index]['data'] = this.copiedItem;
      }
      this.getListData(true);

    } else if ( mode == 'company' ) {

      this.where['company_id'] = this.selectedCompany;
      this.copiedItem = Object.assign([], [] , this.selectedCompany );
      if (!(index !== -1)) {
        this.filteredItem.push({'mode': mode, 'data': this.copiedItem});
      } else {
        this.filteredItem[index]['data'] = this.copiedItem;
      }
      this.getListData(true);

    } else if ( mode == 'role' ) {

      this.where['role'] = this.selectedRole;
      this.copiedItem = Object.assign([], [] , this.selectedRole );
      if (!(index !== -1)) {
        this.filteredItem.push({'mode': mode, 'data': this.copiedItem});
      } else {
        this.filteredItem[index]['data'] = this.copiedItem;
      }
      this.activeMenu = this.selectedRole[0]['id'];
      this.getListData(true);

    } else if ( mode == 'status' ) {

      this.where['status'] = this.selectedStatus;
      this.copiedItem = Object.assign([], [] , this.selectedStatus );
      if (!(index !== -1)) {
        this.filteredItem.push({'mode': mode, 'data': this.copiedItem});
      } else {
        this.filteredItem[index]['data'] = this.copiedItem;
      }
      this.getListData(true);

    }

    this.filterSection[mode] = false;
  }

  clearFilter(mode) {

    if ( mode == 'provider' ) {
      this.where['provider_id'] = [];
      this.selectedProvider = [];
      this.selectedProviderModel = [];
    } else if ( mode == 'company' ) {
      this.where['company_id'] = [];
      this.selectedCompany = [];
      this.selectedCompanyModel = [];
    } else if ( mode == 'status' ) {
      this.selectedStatus = [];
      this.selectedStatusModel = [];
    }

    const index2 = this.filteredItem.findIndex((item) => {
      return item.mode == mode;
    });
    if (index2 !== -1) {
      this.filteredItem.splice(index2, 1);
    }
    this.getListData(true);
  }

  toggleFilter(type, value = true) {
    this.filterSection[type] = value;
  }

  removeFilterItem(mode, index1 = '', isString = false){

    if ( mode == 'provider' ) {
      this.where['provider_id'].splice(index1, 1);
      this.selectedProvider = [];
      this.selectedProviderModel = [];
    } else if ( mode == 'company' ) {
      this.where['company_id'].splice(index1, 1);
      this.selectedCompany = [];
      this.selectedCompanyModel = [];
    } else if ( mode == 'role' ) {
      this.where['role'] = null;
      this.activeMenu = null;
      this.selectedRoleModel = [];
    } else if ( mode == 'status' ) {
      this.where['status'] = null;
      this.selectedStatusModel = [];
    }

    const index2 = this.filteredItem.findIndex((item) => {
      return item.mode == mode;
    });
    if (index2 !== -1) {
      if ( !isString ) {
        this.filteredItem[index2]['data'].splice(index1, 1);
        if ( Object.keys(this.filteredItem[index2]['data']).length <= 0 ) {
          this.filteredItem.splice(index2, 1);
        }
      } else {
        this.filteredItem.splice(index2, 1);
      }
    }
    
    this.getListData(true);
  }
  
  deleteUser(user: any) {
    
    let that = this;
    this.alertService.confirm("Are you sure you want to delete "+user.user_role+" "+user.first_name+" "+user.last_name+"?", true, 'false', function(){
      
      /*if ( user.user_role == 'dealer' || user.user_role == 'subdealer' ) {
        
        that.userData = user;
        that.deleteSide = true;

      } else {*/
        that.dataService.post(that.entityApiPrefix + 'Delete', {'ids': user.user_id})
        .subscribe(users => { 
            if ( users.success ) {
              that.alertService.success(users.message, true);
              that.getListData(true);
            } else {
              that.alertService.error('something went wrong please try again', true); 
            }
        });
      /*}*/

    },function(){
      that.getListData(true);
    });
  }
  
  selectAll() {
    this.selectedUsers = [];
    for (let i = 0; i < this.users.length; i++) {
      this.users[i].selected = this.selectedAll;
      if ( this.selectedAll ) {
        this.selectedUsers.push(this.users[i]['user_id']);
      }
    }
  }
  
  checkIfAllSelected(current_selected_user) {
    this.selectedAll = this.users.every(function(item: any) {
      return item.selected === true;
    });

    if ( this.users[current_selected_user].selected ) {
      this.selectedUsers.push(this.users[current_selected_user]['user_id']);
    } else {

      const contract_index = this.selectedUsers.findIndex((item) => {
        return item == this.users[current_selected_user]['user_id'];
      });
      if (contract_index !== -1) {
        this.selectedUsers.splice(contract_index, 1);
      }
    }
  }
  
  getListData(is_init:boolean = false) {
    
    if ( !this.calling ) {
      this.calling = true;

      if ( is_init ) {
        this.currentPage = 1;
        this.selectedUsers = [];
        if ( document.getElementById("main-panel") != null ) {
          document.getElementById("main-panel").scrollTop = 0;
          if ( this.infiniteScroll != undefined ) {
            this.infiniteScroll.setup();
          }
        }
      }
  
      this.dataService.post(this.entityApiPrefix + 'List', {'where': this.where, 'sort': this.sort, 'pagination' : {'perPageRecords': this.perPageRecords, 'current': this.currentPage}})
        .subscribe(users => { 
  
          if ( is_init ) {
            
            this.totalRecords = users.data.total_count;
            this.totalPages = users.data.page_count;
            if ( users.data.data != undefined ) {
              this.users = users.data.data; 
            } else {
              this.users = [];
            }
  
          } else {
            this.users = this.users.concat(users.data.data);
          }
  
          this.userCount = users.data.user_count;
          this.busy = false;
          this.calling = false;
          this.syncNavHeight();
        }, reject => {
          this.calling = false;
          this.busy = false;
        });
    }
  }

  applyFilter(role = null){
    this.activeMenu = role;
    this.where['role'] = role;
    this.getListData(true);
  }
  
  bulkAction(action) {

    if ( this.actionCheckbox.nativeElement.checked ) {
     this.actionCheckbox.nativeElement.click();
    }

    let selectedUsers = [];
    let actionPath = 'ChangeStatus';
    let singleStatus = '';
    for (let user of this.users) {
      if ( user['selected'] != undefined && user['selected'] ) {
        singleStatus = user['status'];
        selectedUsers.push(user['user_id']);
      }
    }
    
    if ( selectedUsers.length > 0 ) {
      
      if ( selectedUsers.length == 1 && action == singleStatus ) {
        this.alertService.error('Selected user already '+action+'.', true, 'false'); 

      } else {

        let conformMsg = 'Are you sure you want to '+action+' '+selectedUsers.length+' users?';
        if ( action == 'delete' ) {
          actionPath = 'Delete';
          //conformMsg = 'Are you sure you want to delete '+selectedUsers.length+' users?';
        }

        let that = this;
        this.alertService.confirm(conformMsg, true, 'false', function(){
          
          that.dataService.post(that.entityApiPrefix + actionPath, {'ids': selectedUsers, 'action': action})
          .subscribe(users => { 
              if ( users.success ) {
                that.alertService.success(users.message, true);
                
                // Socket Trigger
                that.notificationService.sendEmitter('notifyToAll', {mode: 'user-status-change'});

                that.getListData(true);
                that.selectedAll = false;
              } else {
                that.alertService.error(users.message, true); 
              }
          });

        },function(){
          console.log('NO');
        });
      }

    } else {
      this.alertService.error('Please select atleast one user.', true, 'false'); 
    }

  }

  searchData(event) {
    this.where['string'] = event;
    this.getListData(true);
  }

  sortData(column){
    this.sort[column] = ((this.sort[column] == '') ? 'asc' : (this.sort[column] == 'asc') ? 'desc' : '');
    this.getListData(true);
  }

  changeStatus(is_active, user_id){

    let that = this;
    this.alertService.confirm("Are you sure, You want to change status?", true, 'false', function(){
      
      that.dataService.post(that.entityApiPrefix + 'ChangeStatus', {'ids': user_id, 'action': ((is_active) ? 'active' : 'inactive')})
        .subscribe(users => { 
            that.getListData(true);
        });

    },function(){
      that.getListData(true);
    });
    
  }

  onScroll() {
    if ( !this.calling ) {
      this.currentPage = this.currentPage + 1;
      if ( this.currentPage <= this.totalPages ) {
        this.getListData();
      }
    }
  }

  changePassword(user) {
    this.userData = user;
    this.changePasswordSide = true;
  }

  addUser(user:any = []) {
    if ( this._global.getlocalStorage('user_role') == 'dealer' ) {
      if ( user == [] ) {
         user = {
          'parent_user_id': this.currentUser['id'], 
          'parent_user_name': this.currentUser['firstname'], 
          'provider_id': this.currentUser['provider_id'], 
          'company_id': this.currentUser['company_id'], 
          'provider_name': this.currentUser['provider_name']
        }; 
      } else {
        user['parent_user_id'] = this.currentUser['id'];
        user['parent_user_name'] = this.currentUser['firstname'];
        user['provider_id'] = this.currentUser['provider_id'];
        user['company_id'] = this.currentUser['company_id'];
        user['provider_name'] = this.currentUser['provider_name'];
      }
    }
    
    this.userData = user;
    this.createUserSide = true;
  }

  close(mode) {
    if ( mode == 'password' ) {
      this.changePasswordSide = false;
    } else if ( mode == 'user' ) {
      this.createUserSide = false;
    } else if ( mode == 'delete' ) {
      this.deleteSide = false;
    }
    this.getListData(true);
  }

  ringcentralAction(mode = 'call', user) {
    this.ringCentralService.ringcentralAction(mode, {
      contact_no: user.contact_no
    });
  }

  toggleAction(section){
    this.toggleRow[section] = !this.toggleRow[section];
  }
  
  isString(val) { return typeof val === 'string'; }
}
