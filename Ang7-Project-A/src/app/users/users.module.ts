import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';

import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { UsersComponent } from './users.component';
import { AddUserComponent } from '../aside/add-user/add-user.component';
import { AsideChangePasswordComponent } from '../aside/aside-change-password/aside-change-password.component';
import { DeleteUserComponent } from '../aside/delete-user/delete-user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserNotesComponent } from '../aside/user-notes/user-notes.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UsersRoutingModule,
    FormsModule
  ],
  declarations: [
  	UsersComponent,
  	AddUserComponent,
    AsideChangePasswordComponent,
    DeleteUserComponent,
    UserDetailComponent,
    UserNotesComponent
  	//NavComponent,
  ]
})
export class UsersModule { }
