import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { NavComponent } from '../nav/nav.component';
import { ExpandMenu, PreferencesList, CompanyList } from '../_directives/index';
import { TooltipModule } from 'ng2-tooltip-directive';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { TinymceModule } from 'angular2-tinymce';
import { NgxDocViewerModule } from 'ngx-doc-viewer';

// ==========
import { FormsModule } from '@angular/forms';
import { ProviderNavComponent } from '../providers/provider/provider-nav/provider-nav.component';
import { ProviderSubHeaderComponent } from '../preferences/provider-sub-header/provider-sub-header.component';
import { CompanyNavComponent } from '../companies/company/company-nav/company-nav.component';
import { CompanySubHeaderComponent } from '../companies/company/company-sub-header/company-sub-header.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';

import { ColorPickerModule } from 'ngx-color-picker';
import { Daterangepicker } from 'ng2-daterangepicker';
import { PaymentCalculateComponent } from '../contracts/payment-calculate/payment-calculate.component';
import { NgxMaskModule } from 'ngx-mask';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ChartModule } from 'primeng/chart';
import { SystemLogsComponent } from '../aside/system-logs/system-logs.component';
import { AddAssetComponent } from '../aside/add-asset/add-asset.component';
import { AsidePaymentsDoneComponent } from '../aside/aside-payments-done/aside-payments-done.component';
import { AsidePaymentsComponent } from '../aside/aside-payments/aside-payments.component';
import { AsideSignatureEmailComponent } from '../aside/aside-signature-email/aside-signature-email.component';
import { AsideSignaturePadComponent } from '../aside/aside-signature-pad/aside-signature-pad.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { UploadAttachmentAgreementComponent } from '../aside/upload-attachment-agreement/upload-attachment-agreement.component';
import { UploadAttachmentContractComponent } from '../aside/upload-attachment-contract/upload-attachment-contract.component';
import { CustomerScoreComponent } from '../aside/customer-score/customer-score.component';
import { AddAgreementSettingsComponent } from '../aside/add-agreement-settings/add-agreement-settings.component';


@NgModule({
  declarations: [
    ProviderNavComponent,
    ProviderSubHeaderComponent,
    CompanyNavComponent,
    CompanySubHeaderComponent,
    PaymentCalculateComponent,
  	NavComponent,
    ExpandMenu,
    PreferencesList,
    CompanyList,
    SystemLogsComponent,
    AddAssetComponent,
    AsidePaymentsDoneComponent,
    AsidePaymentsComponent,
    AsideSignatureEmailComponent,
    AsideSignaturePadComponent,
    UploadAttachmentAgreementComponent,
    UploadAttachmentContractComponent,
    CustomerScoreComponent,
    AddAgreementSettingsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TooltipModule,
    InfiniteScrollModule,
    FormsModule,
    AngularMultiSelectModule,
    NgSelectModule,
    ColorPickerModule,
    Daterangepicker,
    NgxMaskModule.forRoot({}),
    AutoCompleteModule,
    ChartModule,
    SignaturePadModule,
    TinymceModule.withConfig({
      auto_focus: false,
      branding: false,
      toolbar: 'preview | forecolor backcolor image | styleselect,formatselect,fontselect,fontsizeselect | tablecontrols | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
      file_picker_types: 'image',
      content_css: 'assets/tinymce/skins/lightgray/styles.css'
    }),
    NgxDocViewerModule
  ],
  exports: [
     NavComponent,
     TooltipModule,
     InfiniteScrollModule,
     FormsModule,
     ExpandMenu,
     PreferencesList,
     CompanyList,
     ProviderNavComponent,
     ProviderSubHeaderComponent,
     CompanyNavComponent,
     CompanySubHeaderComponent,
     PaymentCalculateComponent,
     AngularMultiSelectModule,
     SystemLogsComponent,
     AddAssetComponent,
     AsidePaymentsDoneComponent,
     AsidePaymentsComponent,
     AsideSignatureEmailComponent,
     AsideSignaturePadComponent,
     UploadAttachmentAgreementComponent,
     UploadAttachmentContractComponent,
     CustomerScoreComponent,
     SignaturePadModule,
     NgSelectModule,
     ColorPickerModule,
     Daterangepicker,
     NgxMaskModule,
     AutoCompleteModule,
     ChartModule,
     TinymceModule,
     NgxDocViewerModule,
     AddAgreementSettingsComponent
  ],
  providers: [
  	CommonModule,
  	RouterModule,
    TooltipModule,
    InfiniteScrollModule,
    FormsModule,
    AngularMultiSelectModule,
    NgSelectModule,
    ColorPickerModule,
    Daterangepicker,
    NgxMaskModule,
    AutoCompleteModule,
    ChartModule,
    TinymceModule,
    SignaturePadModule,
    NgxDocViewerModule
  ],
})
export class SharedModule { }
