﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppGlobals } from '../app.global';
import * as _ from "lodash";

@Injectable()
export class AuthGuard implements CanActivate {

    userRole = '';
    permissions = [];
    access = [];
    baiPass = [];
    checkAcc:boolean = false;

    constructor(private router: Router, private _global: AppGlobals) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (localStorage.getItem('currentUser')) {

            if ( route.data != undefined && Object.keys(route.data).length > 0 ) {
                
                this.checkAcc = false;
                this.userRole = this._global.getlocalStorage('user_role');
                this.baiPass = route.data.baiPass as Array<string>;
                this.access = route.data.permissions as Array<string>;

                if ( localStorage.getItem('permissions') != 'undefined' ) {
                  this.permissions = JSON.parse(localStorage.getItem('permissions')); 
                }
                
                //console.log(this.userRole, this.baiPass, this.access, this.permissions);
                //console.log('part 1');
                
                if ( this.baiPass != undefined && Object.keys(this.baiPass).length > 0 ) {
                    //console.log('baiPass', this.baiPass);
                    let that = this;
                    _.forEach(this.baiPass, function(value) {
                        if ( value == that.userRole ) {
                            that.checkAcc = true;
                        }
                    });

                }

                //console.log('part 2');
                if ( !this.checkAcc ) {
                    if( this.permissions && this.access != undefined && Object.keys(this.access).length > 0 ){ 
                        let that = this;
                        _.forEach(this.permissions, function(value, slug) {
                            _.forEach(that.access, function(val, key) {
                                if ( slug == key ) {
                                    _.forEach(val, function(acce) {
                                        if ( value[acce] ) {
                                            that.checkAcc = true;
                                        }
                                    });
                                }
                            });
                        });
                    }
                }

            } else {
                this.checkAcc = true;
            }

            if ( this.checkAcc ) {
                return true;
            } else {
                this.router.navigate(['/errors/401']);
                return false;
            }
            //return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}