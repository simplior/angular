import {Component, OnInit, Inject} from '@angular/core';
import {ActivatedRoute, Router, Params} from '@angular/router';
import { AlertService, AuthenticationService, DataService, NotificationService } from '../../_services/index';
import { DOCUMENT, Title } from '@angular/platform-browser';
import { AppGlobals } from '../../app.global';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styles: [],
  moduleId: module.id.toString(),
})

export class ResetPasswordComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;
  errors: { [index:string] : any } = {}

  currentProvider:any;
  busy = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private dataService: DataService,
    private _global: AppGlobals,
    private title: Title,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.busy = true;
    // reset login status
    this.authenticationService.logout();

    this.dataService.get('providerProfile')
      .subscribe(provider => {
          
            if ( provider.success ) {
                this.currentProvider = provider.data; 

                let pastUser = {};
                pastUser['logo'] = this.currentProvider.logo;
                pastUser['provider_name'] = this.currentProvider.provider_name;
                pastUser['favicon'] = this.currentProvider.favicon;
                pastUser['theme_color'] = this.currentProvider.theme_color;
                //localStorage.setItem('pastUser', JSON.stringify(pastUser));

                //localStorage.setItem('provider_id', this.currentProvider['ID']);

                // overright localStorage data 
                this.changeBranding(provider.data); 
            }
            this.busy = false;

      }, reject => {
          console.log(reject);
          this.busy = false;
      });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    const validateKey = this.route.snapshot.queryParams['key'];

    this.authenticationService.validateKey(validateKey)
      .subscribe(
        data => {
          if (data.success) {
            this.loading = false;
          } else {
            this.loading = false;
            this.alertService.error(data.message, true);
          }
        });
  }

  changeBranding(data){
      // Set dynamic title
      this.title.setTitle(data.provider_name);

      // Set dynamic favicon
      if ( data.favicon != null ) {
          this._document.getElementById('appFavicon').setAttribute('href', data.favicon);
      }

      // Set dynamic primary and dark color
      if ( data.theme_color != null ) {
          let customStyle = "--primary: " + data.theme_color + ";";
          customStyle += " --secondary: " + this._global.adjustBrightness(data.theme_color, -55) + ";";
          document.querySelector("body").style.cssText = customStyle;
      }
  }

  resetPassword() {
    this.errors = {};
    const validateKey = this.route.snapshot.queryParams['key'];
    this.loading = true;
    this.authenticationService.resetPassword( this.model.password , this.model.confirmPassword , validateKey)
    .subscribe(
      data => {
        if (data.success) {
          this.loading = false;
          this.alertService.success(data.message, true);

          // Socket Trigger
          this.notificationService.sendEmitter('notifyToAll', {mode: 'password_changed'});

          this.router.navigate(['/login']);
        } else {
          this.loading = false;
          this.alertService.error(data.message, true);
        }
      }, reject => {
        this.loading = false;
        this.errors = reject.error.error;
      });
  }
}
