﻿import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppGlobals } from '../app.global';
import { DOCUMENT, Title } from '@angular/platform-browser';
import { AlertService, AuthenticationService, DataService, NotificationService } from '../_services/index';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {

    model: any = {};
    loading = false;
    returnUrl: string;
    currentProvider:any;
    pastUser = {};
    remember_me = {};

    busy = false;
    showPassword:boolean = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private dataService: DataService,
        private _global: AppGlobals,
        private title: Title,
        @Inject(DOCUMENT) private _document: HTMLDocument,
        private notificationService: NotificationService) {
        }

    ngOnInit() {
        this.busy = true;
        this.model = {
            'user_role': 'dealer',
            'login_with': 'email',
            'username': '',
            'password': ''
        }
        /*this.pastUser = JSON.parse(localStorage.getItem('pastUser'));
        if ( this.pastUser != undefined ) {
            console.log(this.pastUser);
            this.currentProvider = this.pastUser;
            // overright localStorage data 
            this.changeBranding(this.pastUser); 
        } else {*/

            this.dataService.get('providerProfile')
              .subscribe(provider => {
                  
                    if ( provider.success ) {
                        this.currentProvider = provider.data;

                        let pastUser = {};
                        pastUser['logo'] = this.currentProvider.logo;
                        pastUser['provider_name'] = this.currentProvider.provider_name;
                        pastUser['favicon'] = this.currentProvider.favicon;
                        pastUser['theme_color'] = this.currentProvider.theme_color;
                        //localStorage.setItem('pastUser', JSON.stringify(pastUser));

                        //localStorage.setItem('provider_id', this.currentProvider['ID']);

                        // overright localStorage data 
                        this.changeBranding(provider.data); 

                        // check remember me
                        this.checkRememberMe();

                        this.model.user_role = (this.currentProvider.is_customer_portal ? 'customer' : 'dealer');
                    }

                    this.busy = false;
              }, reject => {
                  console.log(reject);
                  this.busy = false;
              });
        /*}*/
        
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }

    checkRememberMe(){
        this.remember_me = JSON.parse(localStorage.getItem('remember_me'));
        if ( this.remember_me != undefined && this.remember_me != null ) {
            this.model = {
                'user_role': 'dealer',
                'login_with': this.remember_me['login_with'],
                'username': this.remember_me['username'],
                'password': this.remember_me['password'],
                'remember_me': this.remember_me['remember_me'],
            }
        }
    }

    /*logout() {
        return new Promise((resolve, reject) => {
            // reset login status
            this.authenticationService.logout()
                .subscribe( data => {
                        resolve();
                    },
                    error => { 
                        resolve();
                        this.loading = false;
                    }
                );
        });
    }*/

    changeBranding(data){

        // Set dynamic title
        this.title.setTitle(data.provider_name);

        // Set dynamic favicon
        if ( data.favicon != null ) {
            this._document.getElementById('appFavicon').setAttribute('href', data.favicon);
        }

        // Set dynamic primary and dark color
        if ( data.theme_color != null ) {
            let customStyle = "--primary: " + data.theme_color + ";";
            customStyle += " --secondary: " + this._global.adjustBrightness(data.theme_color, -55) + ";";
            document.querySelector("body").style.cssText = customStyle;
        }
    }
    
    login() {
        this.loading = true;
        this.authenticationService.login(this.model.user_role, this.model.username, this.model.password, this.model.remember_me, this.model.login_with)
          .subscribe(
            data => {
              if ( data.success ) {
                this.loading = false;
                
                // Socket Trigger
                this.notificationService.sendEmitter('notifyToAll', {mode: 'login'});

                // this.alertService.success(data.message, true, data.status);
                this.router.navigate([this.returnUrl]);
              } else {
                this.loading = false;
                this.alertService.error(data.message, false);
              }
            },
            error => {
              this.loading = false;
            });
    }

    displayPassword(){
        this.showPassword = !this.showPassword;
    }

    roleChange(){
        this.model.username = '';
        this.model.password = '';
    }
}
