import { Component, OnInit, ElementRef, ViewChild, HostListener, Inject } from '@angular/core';
import { DOCUMENT, Title } from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { User } from '../_models/';
import { UserService } from '../_services/';
import { AppGlobals } from '../app.global';
import { AuthenticationService, DataService, NotificationService } from '../_services/index';
import { DeviceDetectorService } from 'ngx-device-detector';
import { environment } from './../../environments/environment';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styles: []
})
export class NavComponent implements OnInit {
  imageurl = environment.uploadUrl;
  currentUser: User;
  notifications;
  users: User[] = [];
  url;
  message:any;
  fistName;
  lastName;

  currentRole = '';
  maintenance: { [index:string] : any } = {};

  @ViewChild('underDevelopmentFlag') udf: ElementRef;

  systemLogSide = false;
  logCount = 0;
  systemData = {};

  expandMenuFlag = false;
  expandSubFlag = false;
  expandBillSubFlag = false;
  expandContractSubFlag = false;

  isProviderMode:boolean = false;
  invoice_id;
  provider_id;
  investor_company_id;
  provider_data = [];
  deviceType = '';
  uploadUrl: string = '';
  isInvoiceMode = false;
  isInvestorCompanyMode = false;
  isCollapsemenu: boolean = true;

  constructor(@Inject(DOCUMENT) private _document: HTMLDocument, private activatedRoute: ActivatedRoute, private dataService: DataService, private el:ElementRef, private userService: UserService, public _global: AppGlobals, private authenticationService: AuthenticationService, private router: Router, private deviceService: DeviceDetectorService, private notificationService: NotificationService, private title: Title) {
    this.maintenance = JSON.parse(localStorage.getItem('maintenance_mode'));
    this.uploadUrl = environment.uploadUrl;
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.deviceType = (this.deviceService.isDesktop() ? 'desktop' : (this.deviceService.isMobile() ? 'mobile' : (this.deviceService.isTablet() ? 'tablet' : '')));

    //this.updateUser();
    this.systemLogCount();
    this.url = this._global.baseAPIUrl;
    this.currentRole = this._global.getlocalStorage('user_role');
    
    // check current route is provider route or not
    if ( this.router.url.indexOf('providers/') !== -1 ) {
      this.isProviderMode = true;
      this.activatedRoute.params.subscribe((params: Params) => {
        this.provider_id = params.id;

        this.dataService.get('providerProfile')
          .subscribe(provider => {
                  if ( provider.success ) {
                    this.provider_data = provider.data;
                  }
            }, reject => {
            });
      });
    }

    // check current route is invoices route or not
    if ( this.router.url.indexOf('invoices') !== -1 ) {
      this.isInvoiceMode = true;
      this.activatedRoute.params.subscribe((params: Params) => {
        this.invoice_id = params.invid;
      });
    }
    
    // check current route is investor company route or not
    if ( this.router.url.indexOf('investors/') !== -1 ) {
      this.isInvestorCompanyMode = true;
      this.activatedRoute.params.subscribe((params: Params) => {
        this.investor_company_id = params.id;
      });
    }
    
    // comment for stop notification
    // this.notificationService.receivedNotification('notifyToAll').subscribe(data => {
    //   if ( data.mode == 'reload' ) {
    //     window.location.reload();
    //   } else if ( data.mode == 'login' || data.mode == 'forgot-password' || data.mode == 'password_changed' || data.mode == 'contract-init' || data.mode == 'contract-create' || data.mode == 'contract-update' || data.mode == 'contract-status-change' || data.mode == 'contract-side-actions' || data.mode == 'user-create' || data.mode == 'user-update' || data.mode == 'user-status-change' || data.mode == 'provider-create' || data.mode == 'provider-status-change' || data.mode == 'account-default' || data.mode == 'term-change' ) {
    //     this.systemLogCount();
    //   }
		// });
  }

  ngAfterViewInit(){
    //this.syncNavHeight();
  }

  syncNavHeight(){
    
    let extraHeight = 0;
    if ( this.maintenance != undefined && this.maintenance.is_maintenance_mode ) {
      extraHeight = 23;
    }

    $('.page-header').css('top', (this.el.nativeElement.offsetHeight + extraHeight)+'px');
    $('.main-container').css('top', (this.el.nativeElement.offsetHeight + extraHeight + $('.page-header').outerHeight())+'px');
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
      //console.log(window.pageYOffset);
      if (window.pageYOffset > 30) {
        if ( !this._document.body.classList.contains('fixed-header') ) {
          this._document.body.classList.add('fixed-header');
        }
      } else {
        this._document.body.classList.remove('fixed-header');
      }
  }

  /*updateUser(){
    this.userService.currentMessage.subscribe(message => this.currentUser = message);
  }*/
  
  getNotificationsdetails(){
    this.userService.getNotification().subscribe( data => {
      this.notifications = data;
    });
  }

  expandMainMenuFlag() {
    if ( this.isCollapsemenu ) {
      this._document.body.classList.add('menuopen');
    } else {
      this._document.body.classList.remove('menuopen');
    }
    this.isCollapsemenu = !this.isCollapsemenu;
  }

  logout() {
    
    // reset login status
    this.authenticationService.logout()
        .subscribe( data => {
            // Socket Trigger
            this.notificationService.sendEmitter('notifyToAll', {mode: 'login'});
            this.notificationService.close();
            this.router.navigate(['/login']); 
          },
          error => { 
             console.log(error);
          }
        );
  }

  forceReload () {
    // Socket Trigger
    // this.notificationService.sendEmitter('notifyToAll', {mode: 'reload'});
  }

  removeMain(){
    localStorage.removeItem('maintenance_mode');
    document.body.classList.remove('maintenance');
    this.maintenance = {};
    //this.syncNavHeight();
  }

  systemLogCount() {
    this.dataService.get('systemLogCount')
      .subscribe(history => {
          if ( history.success ) {
            this.logCount = history.data.count; 
            
            if ( this.logCount > 0 ) {
              // Set dynamic title
              if(this._global.getlocalStorage('user_role') == 'superadmin') {
                this.title.setTitle('('+((this.logCount <= 99) ? this.logCount : '99+')+') ' + this._global.getlocalStorage('provider_name'));
              } else {
                this.title.setTitle(this._global.getlocalStorage('provider_name'));
              }
            }
          }
      }, reject => {
          console.log(reject.error.error);
      });
  }

  isLinkActive(url: string): boolean {
    return (this.activatedRoute.snapshot['_routerState'].url === url);
  }

  openSystemLog() {
    this.systemLogSide = true;
    this.systemData = {};
  }

  closeSide() {
    this.systemLogCount();
    this.systemLogSide = false;
  }
}
