﻿import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/index';
import { AuthGuard } from './_guards/index';
import { forgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './login/reset-password/reset-password.component';
import { HelpComponent } from './other/help/help.component';
import { PrivacyPolicyComponent } from './other/privacy-policy/privacy-policy.component';
import { SupportComponent } from './other/support/support.component';
import { ActivityComponent } from './other/activity/activity.component';
import { ErrorsComponent } from './errors/errors.component';
import { WebCalculatorComponent } from './web-calculator/web-calculator.component';

const appRoutes: Routes = [
    { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'companies', loadChildren: './companies/companies.module#CompaniesModule'},
    { path: 'contracts', loadChildren: './contracts/contracts.module#ContractsModule'},
    { path: 'agreements', loadChildren: './agreements/agreements.module#AgreementsModule'},
    { path: 'providers', loadChildren: './providers/providers.module#ProvidersModule'},
    { path: 'preferences', loadChildren: './preferences/preferences.module#PreferencesModule'},
    { path: 'help', component: HelpComponent, canActivate: [AuthGuard] },
    { path: 'privacy-policy', component: PrivacyPolicyComponent, canActivate: [AuthGuard] },
    { path: 'support', component: SupportComponent, canActivate: [AuthGuard] },
    { path: 'payment-methods', loadChildren: './payment-methods/payment-methods.module#PaymentMethodsModule'},
    { path: 'activity', component: ActivityComponent, canActivate: [AuthGuard] },
    { path: 'users', loadChildren: './users/users.module#UsersModule'},
    { path: 'assets', loadChildren: './assets/assets.module#AssetsModule'},
    { path: 'products', loadChildren: './products/products.module#ProductsModule'},
    { path: 'coupons', loadChildren: './coupons/coupons.module#CouponsModule'},
    { path: 'investors', loadChildren: './investor-companies/investor-companies.module#InvestorCompaniesModule'},
    { path: 'invoices', loadChildren: './invoices/invoices.module#InvoicesModule'},
    { path: 'bills', loadChildren: './bills/bills.module#BillsModule'},
    { path: 'login', component: LoginComponent },
    { path: 'login/forgot-password', component: forgotPasswordComponent },
    { path: 'login/reset-password', component: ResetPasswordComponent },
    { path: 'web-calculator', component: WebCalculatorComponent },
    { path: 'errors/:type', component: ErrorsComponent },
    { path: '#', redirectTo: '' },
    // otherwise redirect to home
    { path: '**', redirectTo: 'errors/404' }
];

export const routing = RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'});
