﻿import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';

// used to create fake backend
//import { fakeBackendProvider } from './_helpers/index';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptor } from './_helpers/index';
import { AlertService, AuthenticationService, UserService, ContractsService,ProvidersService, DataService, NotificationService, RingcentralService } from './_services/index';
import { LoginComponent } from './login/index';
// import { RegisterComponent } from './register/index';
import { DashboardComponent } from './dashboard/dashboard.component';
import { forgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AsideComponent } from './aside/aside.component';
import { ResetPasswordComponent } from './login/reset-password/reset-password.component';
import { HelpComponent } from './other/help/help.component';
import { SupportComponent } from './other/support/support.component';
import { PrivacyPolicyComponent } from './other/privacy-policy/privacy-policy.component';


import { ActivityComponent } from './other/activity/activity.component';
import { AppGlobals } from './app.global';

import { SharedModule } from './shared/shared.module';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { ErrorsComponent } from './errors/errors.component';

import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { WebCalculatorComponent } from './web-calculator/web-calculator.component'
;
import { SidebarNavComponent } from './sidebar-nav/sidebar-nav.component';

@NgModule({
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        //BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        routing,
        //FilePondModule,
        SharedModule,
        LoadingBarHttpClientModule,
        NgIdleKeepaliveModule.forRoot(),
        DeviceDetectorModule.forRoot()
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        LoginComponent,
        // RegisterComponent,
        forgotPasswordComponent,
        ResetPasswordComponent,
        DashboardComponent,
        AsideComponent,
        HelpComponent,
        SupportComponent,
        PrivacyPolicyComponent,
        ActivityComponent,
        ErrorsComponent,
        WebCalculatorComponent,
        SidebarNavComponent
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        ContractsService,
        UserService,
        ProvidersService,
        DataService,
        NotificationService,
        RingcentralService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        AppGlobals,
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        // provider used to create fake backend
        // fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
