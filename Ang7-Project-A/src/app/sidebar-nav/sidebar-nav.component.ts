import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT, Title } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import { AuthenticationService, DataService, NotificationService } from '../_services/index';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { AppGlobals } from '../app.global';
import { DeviceDetectorService } from 'ngx-device-detector';
import { User } from '../_models/';

@Component({
  selector: 'sidebar-nav',
  templateUrl: './sidebar-nav.component.html'
})
export class SidebarNavComponent implements OnInit {

  imageurl = environment.uploadUrl;
  expandAccountingFlag = false;
  expandAccountingSubFlag = false;
  isCollapse: boolean = true;
  expandManageFlag = false;
  expandContractSubFlag = false;
  url;
  currentRole = '';
  systemLogSide = false;
  logCount = 0;
  systemData = {};
  deviceType = '';
  currentUser: { [index:string] : any } = {};
  maintenance: { [index:string] : any } = {};
  uploadUrl: string = '';
  isProviderMode:boolean = false;

  constructor(@Inject(DOCUMENT) private _document: HTMLDocument, private authenticationService: AuthenticationService, private notificationService: NotificationService, private router: Router, public _global: AppGlobals, private dataService: DataService, private title: Title, private deviceService: DeviceDetectorService, private activatedRoute: ActivatedRoute) { 
    this.maintenance = JSON.parse(localStorage.getItem('maintenance_mode'));
    this.uploadUrl = environment.uploadUrl;
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.deviceType = (this.deviceService.isDesktop() ? 'desktop' : (this.deviceService.isMobile() ? 'mobile' : (this.deviceService.isTablet() ? 'tablet' : '')));
    this.systemLogCount();
    this.url = this._global.baseAPIUrl;
    this.currentRole = this._global.getlocalStorage('user_role');
  }

  expandMainMenuFlag() {
    if ( this.isCollapse ) {
      this._document.body.classList.add('collapse');
    } else {
      this._document.body.classList.remove('collapse');
    }
    this.isCollapse = !this.isCollapse;
  }
  
  closeMainMenuFlag() {
    this._document.body.classList.remove('menuopen');
  }

  logout() {
    
    // reset login status
    this.authenticationService.logout()
        .subscribe( data => {
            // Socket Trigger
            this.notificationService.sendEmitter('notifyToAll', {mode: 'login'});
            this.notificationService.close();
            this.router.navigate(['/login']); 
          },
          error => { 
             console.log(error);
          }
        );
  }

  systemLogCount() {
    this.dataService.get('systemLogCount')
      .subscribe(history => {
          if ( history.success ) {
            this.logCount = history.data.count; 
            
            if ( this.logCount > 0 ) {
              // Set dynamic title
              if(this._global.getlocalStorage('user_role') == 'superadmin') {
                this.title.setTitle('('+((this.logCount <= 99) ? this.logCount : '99+')+') ' + this._global.getlocalStorage('provider_name'));
              } else {
                this.title.setTitle(this._global.getlocalStorage('provider_name'));
              }
            }
          }
      }, reject => {
          console.log(reject.error.error);
      });
  }

  openSystemLog() {
    this.systemLogSide = true;
    this.systemData = {};
  }

  closeSide() {
    this.systemLogCount();
    this.systemLogSide = false;
  }

  isLinkActive(url: string): boolean {
    return (this.activatedRoute.snapshot['_routerState'].url === url);
  }

}
