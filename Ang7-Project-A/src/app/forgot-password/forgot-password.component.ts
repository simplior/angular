import {Component, OnInit, Inject} from '@angular/core';
import { AlertService, AuthenticationService, DataService, NotificationService } from '../_services/index';
import { DOCUMENT, Title } from '@angular/platform-browser';
import { AppGlobals } from '../app.global';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styles: [],
  moduleId: module.id.toString(),
})

export class forgotPasswordComponent implements OnInit {

  model: any = {};
  loading = false;

  currentProvider:any;
  busy = false;

  constructor(
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private dataService: DataService,
    private _global: AppGlobals,
    private title: Title,
    @Inject(DOCUMENT) private _document: HTMLDocument, 
    private notificationService: NotificationService) {
  }

  ngOnInit() {

    this.model = {
        'user_role': 'dealer',
        'login_with': 'email',
        'username': ''
    }

    this.busy = true;

    // reset login status
    this.authenticationService.logout();

    this.dataService.get('providerProfile')
      .subscribe(provider => {
          
            if ( provider.success ) {
                this.currentProvider = provider.data; 

                let pastUser = {};
                pastUser['logo'] = this.currentProvider.logo;
                pastUser['provider_name'] = this.currentProvider.provider_name;
                pastUser['favicon'] = this.currentProvider.favicon;
                pastUser['theme_color'] = this.currentProvider.theme_color;
                //localStorage.setItem('pastUser', JSON.stringify(pastUser));

                //localStorage.setItem('provider_id', this.currentProvider['ID']);

                // overright localStorage data 
                this.changeBranding(provider.data); 

                this.model.user_role = (this.currentProvider.is_customer_portal ? 'customer' : 'dealer');
            }
            this.busy = false;

      }, reject => {
          console.log(reject);
          this.busy = false;
      });
  }

  changeBranding(data){
      // Set dynamic title
      this.title.setTitle(data.provider_name);

      // Set dynamic favicon
      if ( data.favicon != null ) {
          this._document.getElementById('appFavicon').setAttribute('href', data.favicon);
      }

      // Set dynamic primary and dark color
      if ( data.theme_color != null ) {
          let customStyle = "--primary: " + data.theme_color + ";";
          customStyle += " --secondary: " + this._global.adjustBrightness(data.theme_color, -55) + ";";
          document.querySelector("body").style.cssText = customStyle;
      }
  }

  forgotPassword() {
    if( !this.loading ){

      this.loading = true;
      this.authenticationService.forgotPassword(this.model.user_role, this.model.username, this.model.login_with)
        .subscribe(
          data => {
            if (data.success) {
              this.loading = false;
              this.alertService.success(data.message, true);
              
              // Socket Trigger
              this.notificationService.sendEmitter('notifyToAll', {mode: 'forgot-password'});

            } else {
              this.loading = false;
              this.alertService.error(data.message, true);
            }
          });
    }
  }
}
