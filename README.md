### Angular Practices ###

* Use trackBy instead of ngFor
* Lazy Loading Feature Module
* CDK Virtual Scroll
* Angular Coding Styles
* Folder Structure for Angularh
* Prevent Memory Leaks in Angular Observable
* Utilizing ES6 Features
* State Management
* Declare Variable Type Instead of Using ‘any’
* Cache API calls
* AOI build instead of normal build